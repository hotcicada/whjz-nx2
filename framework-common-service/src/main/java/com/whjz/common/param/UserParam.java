package com.whjz.common.param;

import java.io.Serializable;

import lombok.Data;

@Data
public class UserParam implements Serializable {

	private static final long serialVersionUID = -6931931673983801853L;

	private String clientSeqNo;
	
	private String token;
}
