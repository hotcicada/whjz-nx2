package com.whjz.common.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class DictDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	/* 字典名称 */
	private String dictName;

	/* 字典值 */
	private String dictValue;
	
	private String dictTypeCode;

}
