package com.whjz.common.dao;

import com.whjz.entity.MbBankOrg;

import java.util.List;
import java.util.Map;

public interface MbCommonMapper {
    MbBankOrg selectOrgInfoByOrgCode(String orgCode);//根据机构号查询机构信息
    List<MbBankOrg> queryAllSubOrglistByOrgCode(String orgCode);// 查询机构下所有子机构
    int countBusCodeMenu(Map<String,String> map);
}