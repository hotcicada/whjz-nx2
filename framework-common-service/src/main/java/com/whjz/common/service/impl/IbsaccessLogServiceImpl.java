package com.whjz.common.service.impl;

import com.whjz.common.dao.IbsaccessLogForLoginMapper;
import com.whjz.common.dao.MbBackUserMapper;
import com.whjz.common.service.IbsaccessLogService;
import com.whjz.entity.IbsaccessLogForLogin;
import com.whjz.entity.MbBackUser;
import com.whjz.utils.CodeUtil;
import com.whjz.utils.DateUtils;
import com.whjz.utils.String2Utils;
import com.whjz.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

//import com.whjz.common.entity.MbBackUser;


@Service
public class IbsaccessLogServiceImpl implements IbsaccessLogService {

//	@Autowired
//	@SuppressWarnings("all")
//	private IbsaccessLogMapper ibsaccessLogMapper;

    @Autowired
    @SuppressWarnings("all")
    private IbsaccessLogForLoginMapper ibsaccessLogForLoginMapper;

    @Autowired
    @SuppressWarnings("all")
    private MbBackUserMapper mbBackUserMapper;


    @Transactional
    @Override
    public boolean insertIbsaccessLog(Map<String, String> reqBody) {
        /** mod by zhangxueliang for(bug)048  解决 loginResult 为空时的空指针问题 20181105 begin*/
        String clientSeqNo = reqBody.get("custNo");
        IbsaccessLogForLogin log = new IbsaccessLogForLogin();
        if (reqBody.get("loginResult") != null) { // 判断非null
            if (CodeUtil.CODE_200.getCode().equals(reqBody.get("loginResult"))) {//根据code值判断登录成功与否
                String loginState = StringUtils.getString(reqBody.get("loginState"));//如果首次登录 记录首次登录状态
                if (org.apache.commons.lang3.StringUtils.isNotBlank(loginState) && "3".equals(loginState)) {
                    log.setLoginState(loginState);
                } else {
                    log.setLoginState("1");// 是否登录成功： 1-成功 2-失败3-首次登陆
                }
            } else {
                log.setLoginState("2");
            }
        }
        if (reqBody.get("userId") != null) {
            clientSeqNo = reqBody.get("userId");
        }
        /** mod by zhangxueliang for(bug)048 解决 loginResult 为空时的空指针问题 20181105 end*/
        String userId = clientSeqNo;
        MbBackUser backUser = mbBackUserMapper.selectByUserId(userId);
        if(backUser!=null){
            log.setOrgId(backUser.getOrgId());
        }
        String pushDeviceId = reqBody.get("pushDeviceId");
        String deviceId = reqBody.get("deviceId");
        String version = reqBody.get("version");
        String network = reqBody.get("network");
        String wifiName = reqBody.get("wifiName");
        String deviceType = reqBody.get("deviceType");
        String address = reqBody.get("address");
        String longitude = reqBody.get("longitude");
        String latitude = reqBody.get("latitude");
        String ext1 = reqBody.get("ext1");
        log.setRecordId(String2Utils.getGenerateUUID());
        log.setLoginDate(DateUtils.getCurDateTime());
        log.setWifiName(wifiName);
        log.setVersion(version);
        log.setUserId(userId);
        log.setPushDeviceId(pushDeviceId);
        log.setNetwork(network);
        log.setLongitude(longitude);
        log.setLatitude(latitude);
        log.setAddress(address);
        log.setDeviceType(deviceType);
        log.setDeviceId(deviceId);
        log.setClientNo(clientSeqNo);
        log.setExt1(ext1);
        log.setExt2(reqBody.get("ext2"));
        log.setExt3(reqBody.get("ext3"));
        log.setExt4(reqBody.get("ext4"));
        log.setExt5(reqBody.get("ext5"));
        log.setCreateDate(DateUtils.getCurDateTime());
        log.setUpdateDate(DateUtils.getCurDateTime());
        int count = ibsaccessLogForLoginMapper.insertLogSelective(log);
//		int count = ibsaccessLogMapper.insertLogSelective(log);更新为新的mapper
        return count > 0;
    }
}
