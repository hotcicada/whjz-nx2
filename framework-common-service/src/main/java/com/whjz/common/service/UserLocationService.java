package com.whjz.common.service;

import java.util.Map;

public interface UserLocationService {
    Map queryList(Map<String, String> req) ;
    Map saveUserLocation(Map<String, String> req);
    Map queryAllList(Map<String, String> req) ;
}
