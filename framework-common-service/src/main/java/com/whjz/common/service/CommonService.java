package com.whjz.common.service;
import java.util.List;
import java.util.Map;

public interface CommonService {
	
	
	
	

	/**
	 * 获取配置列表
	 * @return
	 */
	Map<String,Object> getListConfig();
    
    /**
     * 获取字典集合
     * @return
     */
	Map<String,Object> getListDict();


	/**
	 * 获取字典集合
	 * @return
	 */
	List queryDictByKey(Map<String,String > req);

	/**
	 * 获取最大后台流水号
	 * @param req
	 * @return
	 */
	String queryTheMaxBackReqNO(Map<String,String > req);

	/**
	 * 校验用户密码
	 * @param req
	 * @return
	 */
	Map<String,Object> checkTheUserPasswd(Map<String,String > req);

	/**
	 * 重置密码
	 * @param req
	 * @return
	 */
	Map<String,Object> resetPasswd(Map<String,String > req);

	/**
	 * 设置默认密码
	 * @param req
	 * @return
	 */
	Map<String,Object> resetDefaultPasswd(Map<String,String > req);



	Map<String,Object> login(Map<String,String > req);//登录

	Map<String,Object> selectUserInfoByUserId(Map<String,String > req);//根据userId查询用户信息


    Map<String, Object> loginOut(Map<String, String> reqBody);//登录退出
}
