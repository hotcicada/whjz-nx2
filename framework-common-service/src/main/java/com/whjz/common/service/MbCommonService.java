package com.whjz.common.service;

import java.util.Map;

public interface MbCommonService {
    public Map queryOrgLimit(String loginUserOrgId, String inputOrgId);//通过登录的机构号和输入机构号 查看有无登录权限  返回orgCode的list用于查询;

    public Map<String, Object> countBusCodeMenu(Map<String,String > req);
}
