package com.whjz.common.service.impl;

import com.whjz.common.dao.MbCommonMapper;
import com.whjz.common.service.MbCommonService;
import com.whjz.entity.MbBankOrg;
import com.whjz.utils.CodeUtil;
import com.whjz.utils.ResultCode;
import com.whjz.utils.ResultUtil;
import com.whjz.utils.StringConstantUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service(value = "MbCommonService")
@Slf4j
@SuppressWarnings({"rawtypes", "unchecked"})
public class MbCommonServiceImpl implements MbCommonService {

    @SuppressWarnings("all")
    @Autowired
    private MbCommonMapper mbCommonMapper;


    /**
     * @desc :查询机构权限
     * @author :gzx
     * @date: 2019/9/27 17:56
     * @Param: @Return:
     */
    @Override
    public Map queryOrgLimit(String loginUserOrgId, String inputOrgCode) {
        Map resultReturnMap = new HashMap();
        List<String> orgCodeList = new ArrayList();
        try {
            if (org.apache.commons.lang3.StringUtils.isNotBlank(loginUserOrgId)) {
                MbBankOrg mbBankOrg = mbCommonMapper.selectOrgInfoByOrgCode(loginUserOrgId);
                if (mbBankOrg != null) {
                    List<MbBankOrg> mbBankOrgList = mbCommonMapper.queryAllSubOrglistByOrgCode(mbBankOrg.getOrgCode());//查询子机构
                    if (!mbBankOrgList.isEmpty()) {
                        for (MbBankOrg bankOrg : mbBankOrgList) {
                            orgCodeList.add(bankOrg.getOrgCode());
                        }
                        //如果输入机构号，按照机构号中数据查询
                        if (org.apache.commons.lang3.StringUtils.isNotBlank(inputOrgCode)) {
                            if (orgCodeList.contains(inputOrgCode)) {//有权限查看
                                orgCodeList = new ArrayList();
                                List<MbBankOrg> inputOrgIdOrgList = mbCommonMapper.queryAllSubOrglistByOrgCode(inputOrgCode);//子机构下信息
                                for (MbBankOrg inputOrg : inputOrgIdOrgList) {
                                    orgCodeList.add(inputOrg.getOrgCode());
                                }
                                resultReturnMap.put(StringConstantUtil.ORG_LIMIT, StringConstantUtil.ORG_LIMT_STATUS_YES);
                                resultReturnMap.put(StringConstantUtil.ORG_CODE_LIST, orgCodeList);//机构号集合
                            } else {//无权限查看
                                resultReturnMap.put(StringConstantUtil.ORG_LIMIT, StringConstantUtil.ORG_LIMT_STATUS_NO);
                            }
                        } else {
                            resultReturnMap.put(StringConstantUtil.ORG_LIMIT, StringConstantUtil.ORG_LIMT_STATUS_YES);//有权限查看
                            resultReturnMap.put(StringConstantUtil.ORG_CODE_LIST, orgCodeList);
                        }
                    }else {
                        resultReturnMap.put(StringConstantUtil.ORG_LIMIT, StringConstantUtil.ORG_LIMT_STATUS_NO);//无权限查看
                    }
                }
            } else {
                resultReturnMap.put(StringConstantUtil.ORG_LIMIT, StringConstantUtil.ORG_LIMT_STATUS_NO);//无权限查看
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return resultReturnMap;
    }

    /**
     * @desc :查询机构权限
     * @author :gzx
     * @date: 2019/9/27 17:56
     * @Param: @Return:
     */
    @Override
    public Map<String, Object> countBusCodeMenu(Map<String, String> req) {
        Map resultReturnMap = new HashMap();
        try {
            int count = mbCommonMapper.countBusCodeMenu(req);
            if(count==0){
                return ResultUtil.resultMap("该交易暂停服务", CodeUtil.CODE_500.getCode(), null);
            }else{
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap("权限异常", CodeUtil.CODE_500.getCode(), null);
    }
}
