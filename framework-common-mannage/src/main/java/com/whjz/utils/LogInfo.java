package com.whjz.utils;

import com.google.common.base.Preconditions;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 日志打印工具
 */
@Slf4j
public class LogInfo {

    /**
     * Controller接口打印，在重要的地方打印
     * @param req
     */
    public static void ControllerLogInfo(Map req) {
        log.info("=======>调用接口开始：日志流水号："+req.get("logseq")+"======>前端参数是："+req);
    }

    /**
     * 实现类启动
     * @param desc
     */
    public static void serviceImplLogInfo(String desc) {
        log.info("===>"+desc+":");
    }

    /**
     *
     * @param msg
     * @param ResultCode
     * @param data
     */
    public static void ResultLogInfo(String msg,String ResultCode,Object data) {
        log.info("\n========>"+msg+"==>"+ResultCode+"==>"+ data);
    }

    /**
     * 常用
     * @param data
     */
    public static void ResultNormalSuccess(Object data) {
        log.info("\n========>查询成功==>200==>"+ data);
    }
    /**
     * 常用
     * @param data
     */
    public static void ResultNormalError(Object data) {
        log.info("\n========>查询失败==>500==>"+ data);
    }
}
