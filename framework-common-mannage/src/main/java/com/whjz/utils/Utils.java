package com.whjz.utils;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Utils {
    public static ObjectMapper MAPPER = new ObjectMapper();

    /**
     * 获取第一个元素(包含)
     *
     * @param page     从1开始
     * @param pageSize >=1
     * @return 从0开始
     */
    public static int getFirstElement(int page, int pageSize) {
        return (page - 1) * pageSize;
    }

    /**
     * 获取最后一个元素(包含)
     *
     * @param page     从1开始
     * @param pageSize >=1
     * @return 从0开始
     */
    public static int getEndElement(int page, int pageSize) {
        return getFirstElement(page, pageSize) + pageSize - 1;
    }

    /**
     * @return 从1开始,0表示无
     */
    public static int getTotalPage(int total, int pageSize) {
        if (total <= 0) {
            return 0;
        }
        return (total-1)/pageSize+1;
    }

    public static List getPage(List list, int pageNum, int pageSize) {
        int first = getFirstElement(pageNum, pageSize);
        int end = getEndElement(pageNum, pageSize);
        List result = new ArrayList();
        for (int i=first;i<=end;i++) {
            if (i >= 0 && i < list.size()) {
                result.add(list.get(i));
            }
        }
        return result;
    }

    public static List getPage(List list, int pageStart, int pageEnd, int pageSize) {
        int first = getFirstElement(pageStart, pageSize);
        int end = getEndElement(pageEnd, pageSize);
        List result = new ArrayList();
        for (int i=first;i<=end;i++) {
            if (i >= 0 && i < list.size()) {
                result.add(list.get(i));
            }
        }
        return result;
    }

    /**
     * 单一元素的map
     *
     * @param key   键
     * @param value 值
     * @return map
     */
    public static Map newMap(Object key, Object value) {
        Map map = new HashMap();
        map.put(key, value);
        return map;
    }

    public static JSONObject getSnResult(JSONObject json) {
        if (json.containsKey("returnCode")) {
            json.remove("returnCode");
        }
        return json;
    }

    /**
      * @author ： lcc
      *  @DEsc :把json转化成String
      *  @date: 2019/5/5 8:59
       * @Param: null
      * @return
      */
    public static String  toJSONString(Object obj ){

        return JSONObject.toJSONString(obj, SerializerFeature.WriteMapNullValue);
    }

    public static <T extends Object> T getParamDTO(Map<String, String> req, Class<T> clazz) {
        try {
            return JSONObject.parseObject(JSONObject.toJSONString(req), clazz);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public static <T extends Object> T getParamDTO(Object req, Class<T> clazz) {
        try {
            return JSONObject.parseObject(JSONObject.toJSONString(req), clazz);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
      * @author ： lcc
      *  @DEsc :验证是否符合身份证表中
      *  @date: 2019/5/13 19:58
       * @Param: null
      * @return
      */
    public static boolean cardCodeVerifySimple(String cardcode) {
        //第一代身份证正则表达式(15位)
        String isIDCard1 = "^[1-9]\\d{7}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}$";
        //第二代身份证正则表达式(18位)
        String isIDCard2 ="^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])((\\d{4})|\\d{3}[A-Z])$";

        //验证身份证
        if (cardcode.matches(isIDCard1) || cardcode.matches(isIDCard2)) {
            return true;
        }
        return false;
    }

    /**
      * @author ： lcc
      *  @DEsc :判断是否为客户号
     *  客户号规则，10位且为数字
      *  @date: 2019/5/13 19:59
       * @Param: null
      * @return
      */
    public static boolean isCustNo(String custNo){
        Pattern pattern = Pattern.compile("[0-9]*");

        if(custNo.length() != 10 || !pattern.matcher(custNo).matches() ){
            return  false;
        }
        return  true;
    }

    /**
     * 切除url前的host,port
     * @param url
     * @return 如'https://www.baidu.com/path'返回'/path'
     */
    public static String trimHost(String url) {
        try {
            if (url == null) {
                return url;
            }
            String trimScheme = url.substring(url.indexOf("//")+2);
            return trimScheme.substring(trimScheme.indexOf("/"));
        } catch (Exception e) {
            e.printStackTrace();
            return url;
        }
    }

    /**
     * 获取计息方式名称
     * @param jxfs 编码
     * @return 可为null
     */
    public static String getJxfsName(String jxfs) {
        if ("001".equals(jxfs)) {
            return "申请人付息";
        }
        if ("002".equals(jxfs)) {
            return "卖方付息";
        }
        if ("003".equals(jxfs)) {
            return "协议付息";
        }
        return jxfs;
    }

    /**
     * 获取计息方式名称
     * @return 可为null
     */
    public static String getPCA0000057Name(String PCA0000057) {
        if ("0".equals(PCA0000057)) {
            return "不计息";
        }
        if ("1".equals(PCA0000057)) {
            return "按月计息";
        }
        if ("2".equals(PCA0000057)) {
            return "按季结息";
        }
        if ("5".equals(PCA0000057)) {
            return "利随本清";
        }
        if ("6".equals(PCA0000057)) {
            return "灵活计息";
        }
        if ("7".equals(PCA0000057)) {
            return "按日计息";
        }
        return PCA0000057;
    }

    /**
     * 获取还款方式名称
     * @param code
     * @return
     */
    public static String getHuankuan(String code) {
        if ("0001".equals(code)) {
            return "到期还本付息";
        }
        if ("0002".equals(code)) {
            return "分期还款";
        }
        if ("0003".equals(code)) {
            return "等额本息";
        }
        if ("0004".equals(code)) {
            return "等额本金";
        }
        if ("0005".equals(code)) {
            return "按季还本付息";
        }
        if ("0006".equals(code)) {
            return "按月还款";
        }
        if ("0007".equals(code)) {
            return "灵活还款";
        }
        if ("0008".equals(code)) {
            return "先本金后利息";
        }
        return code;
    }

    /**
     * 获取贷款用途类型
     */
    public static String getLoanUseType(String loanUseType) {
        if ("0".equals(loanUseType)) {
            return "企业客户";
        }
        if ("1".equals(loanUseType)) {
            return "个人客户";
        }

        String prefix = loanUseType.substring(0, 1);//0企业 1个人
        String suffix = loanUseType.substring(1);

        if ("01".equals(suffix)) {
            return "企业流动资金贷款";
        }
        if ("02".equals(suffix)) {
            return "企业固定资产贷款";
        }
        if ("11".equals(suffix)) {
            return "个人生产经营性贷款";
        }
        if ("12".equals(suffix)) {
            return "个人固定资产贷款";
        }
        if ("13".equals(suffix)) {
            return "个人消费贷款";
        }

        return loanUseType;
    }
    /**
     * 获取贷款用途
     */
    public static String getLoanUse(String loanUseType) {
        String name = "";
        String pk = "";
        if(loanUseType.length() >= 3){
            pk = loanUseType.substring(0,3);
            if("101".equals(pk)){
                name = "个人其他类资金需求";
            }else if("102".equals(pk)){
                name = "个人生产经营资金需求";
            }else if("103".equals(pk)){
                name = "个人消费需求";
            }else if("104".equals(pk)){
                name = "企业固定资产资金需求";
            }else if("105".equals(pk)){
                name = "企业流动资金需求";
            }
        }
        if(loanUseType.length() >= 6){
            name += "--";

            pk = loanUseType.substring(3,6);
            if("202".equals(pk)){
                name += "农村建房";
            }else if("205".equals(pk) || "211".equals(pk) || "227".equals(pk)){
                name += "其他";
            }else if("204".equals(pk)){
                name += "房屋、商铺装修";
            }else if("206".equals(pk)){
                name += "原材料、商品、设备采购";
            }else if("209".equals(pk)){
                name += "资金周转";
            }else if("208".equals(pk)){
                name += "对外投资";
            }else if("207".equals(pk)){
                name += "归还借款";
            }else if("210".equals(pk) || "226".equals(pk)){
                name += "其他费用支出";
            }else if("216".equals(pk)){
                name += "资金周转";
            }else if("215".equals(pk)){
                name += "旅行支出";
            }else if("214".equals(pk)){
                name += "教育支出";
            }else if("213".equals(pk)){
                name += "婚嫁支出";
            }else if("212".equals(pk)){
                name += "购车";
            }else if("231".equals(pk)){
                name += "装修";
            }else if("230".equals(pk)){
                name += "建房";
            }else if("229".equals(pk)){
                name += "购买安置房";
            }else if("228".equals(pk)){
                name += "个人消费";
            }else if("218".equals(pk)){
                name += "技术改造";
            }else if("221".equals(pk)){
                name += "商铺装修";
            }else if("220".equals(pk)){
                name += "厂房新建、改造、扩建";
            }else if("219".equals(pk)){
                name += "设备购置";
            }else if("223".equals(pk)){
                name += "原材料、商品采购";
            }else if("225".equals(pk)){
                name += "对外投资";
            }else if("226".equals(pk)){
                name += "归还借款";
            }else{
                name += pk;
            }
        }
        if(loanUseType.length() >= 9){
            pk = loanUseType.substring(6,9);
            if("304".equals(pk) || "312".equals(pk)){
                name += "同行业投资";
            }else if("305".equals(pk) || "313".equals(pk) ){
                name += "跨行业投资";
            }else if("301".equals(pk) || "309".equals(pk) ){
                name += "私人借款";
            }else if("303".equals(pk) || "311".equals(pk) ){
                name += "我行贷款";
            }else if("302".equals(pk) || "310".equals(pk) ){
                name += "他行贷款";
            }else if("306".equals(pk) || "314".equals(pk) ){
                name += "支付工资";
            }else if("308".equals(pk)){
                name += "店面装修";
            }else if("307".equals(pk)|| "315".equals(pk) ){
                name += "支付租金";
            }else{
                name += pk;
            }
        }
        return name;
    }

    /**
     * 获取担保关系名称
     */
    public static String getGuaranteeRelationName(String guaranteeRelation) {
        if ("01".equals(guaranteeRelation)) {
            return "亲戚";
        }
        if ("02".equals(guaranteeRelation)) {
            return "朋友";
        }
        if ("03".equals(guaranteeRelation)) {
            return "供求关系";
        }
        if ("04".equals(guaranteeRelation)) {
            return "集团内企业及内部人员";
        }
        if ("05".equals(guaranteeRelation)) {
            return "互保关系";
        }
        if ("99".equals(guaranteeRelation)) {
            return "其他关系";
        }
        return guaranteeRelation;
    }

    /**
     * 获取关联关系类型名称
     */
    public static String get11003000032_06Relation(String relationshipType) {
        if ("00".equals(relationshipType)) {
            return "其他";
        }
        if ("01".equals(relationshipType)) {
            return "本人";
        }
        if ("010106".equals(relationshipType)) {
            return "法人与法定代表人的直系亲属";
        }
        if ("010201".equals(relationshipType)) {
            return "单一项目股东";
        }
        if ("010202".equals(relationshipType)) {
            return "出资占其净资产30%(含)以上股东";
        }
        if ("010301".equals(relationshipType)) {
            return "相同股东法人";
        }
        if ("010302".equals(relationshipType)) {
            return "相同实际控制人法人";
        }
        if ("010303".equals(relationshipType)) {
            return "同一家族成员控制法人";
        }
        if ("010402".equals(relationshipType)) {
            return "投资额占其净资产30%(含)以上合伙人";
        }
        if ("010501".equals(relationshipType)) {
            return "借名贷款";
        }
        if ("020101".equals(relationshipType)) {
            return "互保(含他行)";
        }
        if ("020401".equals(relationshipType)) {
            return "客群成员风险传导";
        }
        if ("030201".equals(relationshipType)) {
            return "存款协储员";
        }
        if ("02".equals(relationshipType)) {
            return "夫妻";
        }
        if ("03".equals(relationshipType)) {
            return "法定代表人";
        }
        if ("032".equals(relationshipType)) {
            return "法定代表人配偶";
        }
        if ("04".equals(relationshipType)) {
            return "父母与子女";
        }
        if ("05".equals(relationshipType)) {
            return "合伙人";
        }
        if ("06".equals(relationshipType)) {
            return "单位与单位关键人";
        }
        if ("99".equals(relationshipType)) {
            return "其他";
        }
        return null;
    }

    /**
     * 获取共同借款人关系
     */
    public static String getCommonBorrowRelationName(String commonBorrowRelation) {
        if ("01".equals(commonBorrowRelation)) {
            return "本人";
        }
        if ("02".equals(commonBorrowRelation)) {
            return "配偶";
        }
        if ("03".equals(commonBorrowRelation)) {
            return "法人与法定代表人";
        }
        if ("04".equals(commonBorrowRelation)) {
            return "父母与子女";
        }
        if ("010105".equals(commonBorrowRelation)) {
            return "法人与法定代表人配偶";
        }
        if ("010106".equals(commonBorrowRelation)) {
            return "法人与法定代表人的直系亲属";
        }
        if ("010107".equals(commonBorrowRelation)) {
            return "法人与实际控制人";
        }
        if ("010108".equals(commonBorrowRelation)) {
            return "法人与实际控制人配偶";
        }
        if ("010109".equals(commonBorrowRelation)) {
            return "法人与实际控制人直系亲属";
        }
        if ("010110".equals(commonBorrowRelation)) {
            return "法人与控股股东";
        }
        if ("010111".equals(commonBorrowRelation)) {
            return "法人与控股股东配偶";
        }
        if ("010112".equals(commonBorrowRelation)) {
            return "法人与实际控制人直系亲属";
        }
        if ("010201".equals(commonBorrowRelation)) {
            return "单一项目股东";
        }
        if ("010202".equals(commonBorrowRelation)) {
            return "出资占其净资产30%(含)以上股东";
        }
        if ("010301".equals(commonBorrowRelation)) {
            return "相同股东法人";
        }
        if ("010302".equals(commonBorrowRelation)) {
            return "相同实际控制人法人";
        }
        if ("010303".equals(commonBorrowRelation)) {
            return "同一家族成员控制法人";
        }
        if ("010401".equals(commonBorrowRelation)) {
            return "单一项目合伙人";
        }
        if ("010402".equals(commonBorrowRelation)) {
            return "投资额占其净资产30%(含)以上合伙人";
        }
        if ("010403".equals(commonBorrowRelation)) {
            return "情况复杂合伙人";
        }
        if ("010404".equals(commonBorrowRelation)) {
            return "无主业经营合伙人与项目牵头人";
        }
        if ("010501".equals(commonBorrowRelation)) {
            return "借名贷款";
        }
        if ("020101".equals(commonBorrowRelation)) {
            return "互保(含他行)";
        }
        if ("020201".equals(commonBorrowRelation)) {
            return "员工合伙投向企业项目";
        }
        if ("020202".equals(commonBorrowRelation)) {
            return "向员工集资";
        }
        if ("020301".equals(commonBorrowRelation)) {
            return "供应链风险传导";
        }
        if ("020401".equals(commonBorrowRelation)) {
            return "客群成员风险传导";
        }
        if ("030201".equals(commonBorrowRelation)) {
            return "存款协储员";
        }
        if ("030301".equals(commonBorrowRelation)) {
            return "客户作为担保人";
        }
        if ("030302".equals(commonBorrowRelation)) {
            return "客户作为共同借款人";
        }
        if ("030303".equals(commonBorrowRelation)) {
            return "客户作为关联关系人";
        }
        if ("040101".equals(commonBorrowRelation)) {
            return "财务人员";
        }
        if ("040102".equals(commonBorrowRelation)) {
            return "高管";
        }
        if ("040199".equals(commonBorrowRelation)) {
            return "其他";
        }
        return commonBorrowRelation;
    }

    /**
     * 获取业务发生类型名称
     */
    public static String getOccureTypeName(String occureType) {
        if ("0".equals(occureType)) {
            return "只申请主卡";
        }
        if ("1".equals(occureType)) {
            return "同时申请主副卡";
        }
        if ("2".equals(occureType)) {
            return "只申请副卡";
        }
        if ("3".equals(occureType)) {
            return "大额分期申请";
        }
        return occureType;
    }

    /**
     * 获取转化对象名称
     */
    public static String getConvertObjectName(String convertObject) {
        if ("00".equals(convertObject)) {
            return "本人";
        }
        if ("01".equals(convertObject)) {
            return "共同借款人";
        }
        if ("02".equals(convertObject)) {
            return "担保人";
        }
        if ("03".equals(convertObject)) {
            return "亲属";
        }
        if ("04".equals(convertObject)) {
            return "债务人(关联)";
        }
        if ("05".equals(convertObject)) {
            return "企业";
        }
        if ("06".equals(convertObject)) {
            return "合伙人/股东";
        }
        if ("07".equals(convertObject)) {
            return "朋友";
        }
        if ("08".equals(convertObject)) {
            return "其他";
        }
        return convertObject;
    }

    /**
     * 获取抵押物类型名称
     */
    public static String getDyName(String code) {
        if ("01".equals(code)) {
            return "房地产";
        }
        if ("02".equals(code)) {
            return "土地使用权";
        }
        if ("03".equals(code)) {
            return "机动车辆";
        }
        if ("04".equals(code)) {
            return "船舶";
        }
        if ("05".equals(code)) {
            return "机器设备";
        }
        if ("06".equals(code)) {
            return "商铺";
        }
        if ("09".equals(code)) {
            return "农村承包土地经营权";
        }
        if ("11".equals(code)) {
            return "林权";
        }
        if ("99".equals(code)) {
            return "其他";
        }
        return code;
    }

    /**
     * 获取质押物类型名称
     */
    public static String getZyName(String code) {
        if ("01".equals(code)) {
            return "本行存单";
        }
        if ("02".equals(code)) {
            return "商业汇票";
        }
        if ("03".equals(code)) {
            return "机器设备";
        }
        if ("04".equals(code)) {
            return "国债";
        }
        if ("05".equals(code)) {
            return "保证金";
        }
        if ("06".equals(code)) {
            return "仓单/提单";
        }
        if ("07".equals(code)) {
            return "公司股权";
        }
        if ("08".equals(code)) {
            return "备用信用证";
        }
        if ("09".equals(code)) {
            return "应收帐款权利";
        }
        if ("10".equals(code)) {
            return "收费权利";
        }
        if ("11".equals(code)) {
            return "投资理财保单";
        }
        if ("12".equals(code)) {
            return "商铺使用权";
        }
        if ("13".equals(code)) {
            return "出口退税";
        }
        if ("14".equals(code)) {
            return "财运一本通";
        }
        if ("15".equals(code)) {
            return "稳进账";
        }
        if ("16".equals(code)) {
            return "白银质押";
        }
        if ("17".equals(code)) {
            return "信用证（国内证）应收账款";
        }
        if ("99".equals(code)) {
            return "其他";
        }
        return code;
    }

    /**
     * 获取房地产类型名称
     */
    public static String getHouseTypeName(String code) {
        if ("01".equals(code)) {
            return "普通住宅";
        }
        if ("02".equals(code)) {
            return "写字楼";
        }
        if ("03".equals(code)) {
            return "别墅";
        }
        if ("06".equals(code)) {
            return "农村住房财产权";
        }
        if ("07".equals(code)) {
            return "厂房";
        }
        if ("08".equals(code)) {
            return "其他";
        }
        if ("09".equals(code)) {
            return "普通住宅（非唯一住房）";
        }
        if ("10".equals(code)) {
            return "普通住宅（唯一住房）";
        }
        if ("11".equals(code)) {
            return "光地";
        }
        if ("12".equals(code)) {
            return "商铺";
        }
        if ("99".equals(code)) {
            return "无";
        }
        return code;
    }

    public static void main(String... args) {
        Map m = new HashMap();
        List list = null;
        m.put("12","ss");
        m.put("112",list);

        list =  Lists.newArrayList(list);
        System.out.println(JSONObject.toJSONString(m, SerializerFeature.WriteMapNullValue));

//        System.out.println(getLoanUse("101202313"));
    }
}
