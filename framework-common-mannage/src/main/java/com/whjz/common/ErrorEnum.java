package com.whjz.common;

/**
 * 错误
 * @see CommonRuntimeException
 */
public enum ErrorEnum {
    Error("异常"),
    ;

    private String msg;

    ErrorEnum(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
