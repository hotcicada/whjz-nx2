package com.whjz.common.converters;

import com.google.common.base.Strings;
import com.whjz.common.IConverter;
import org.springframework.stereotype.Component;

/**
 * 枢纽的月利率转年利率
 */
@Component
public class SnRateConverter implements IConverter<String, String> {
    @Override
    public String convert(String from, String... args) {
        if (Strings.isNullOrEmpty(from)) {
            return from;
        }

        Double v = Double.parseDouble(from);
        v *= 12;
        v /= 10;
        return v+"";
    }
}
