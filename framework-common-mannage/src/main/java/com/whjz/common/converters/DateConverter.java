package com.whjz.common.converters;

import com.google.common.base.Strings;
import com.whjz.common.IConverter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * 日期转换器
 */
@Component
public class DateConverter implements IConverter<String, String> {
    @Override
    public String convert(String from, String... args) {
        if (Strings.isNullOrEmpty(from)) {
            return from;
        }

        try {
            String fromFormat = args[0];
            String toFormat = args[1];
            return new SimpleDateFormat(toFormat).format(new SimpleDateFormat(fromFormat).parse(from));
        } catch (ParseException e) {
            e.printStackTrace();
            return from;
        }
    }
}
