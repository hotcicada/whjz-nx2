package com.whjz.common.converters;

import com.google.common.base.Strings;
import com.whjz.common.IConverter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * esb的日期转换器
 */
@Component
public class YYYYMMDDConverter implements IConverter<String, String> {
    @Override
    public String convert(String from, String... args) {
        if (Strings.isNullOrEmpty(from)) {
            return from;
        }

        try {
            return new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("yyyyMMdd").parse(from));
        } catch (ParseException e) {
            e.printStackTrace();
            return from;
        }
    }
}
