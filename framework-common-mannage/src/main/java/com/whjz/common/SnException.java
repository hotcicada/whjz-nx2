package com.whjz.common;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

/**
 * 枢纽请求异常
 */
@Data
public class SnException extends CommonException{
    /**
     * 响应代码(非"success"一般表示失败)
     */
    private String code;
    /**
     * 错误原因
     */
    private String msg;
    /**
     * 原始json
     */
    private JSONObject json;

    public SnException(String code, String msg, JSONObject json) {
        this.code = code;
        this.msg = msg;
        this.json = json;
    }

    public SnException(ErrorEnum errorEnum, String code, String msg, JSONObject json) {
        super(errorEnum);
        this.code = code;
        this.msg = msg;
        this.json = json;
    }
}
