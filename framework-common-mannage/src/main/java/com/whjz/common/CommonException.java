package com.whjz.common;

/**
 * 通用异常(非运行时)
 */
public class CommonException extends Exception{
    public CommonException() {
        this(ErrorEnum.Error);
    }

    public CommonException(ErrorEnum errorEnum) {
        super(errorEnum.getMsg());
    }
}
