package com.whjz.common;

/**
 * 转换器接口
 */
public interface IConverter<F, T> {
    /**
     * 转换
     * @param from 来源,注意为null或空值的情况
     * @param args 参数列表
     * @return 结果
     */
    T convert(F from, String... args);
}
