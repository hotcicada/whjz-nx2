package com.whjz.services;

import com.whjz.entity.MbRequestLog;

import java.util.Map;

public interface RequestService {
	/**
	 * @Desc 添加日志请求流水记录  @Author :gzx    @Date: 2019/12/19 16:43
	 */
	public Map addRequestLog(MbRequestLog requestLog);

	public String queryTheMaxBackReqNO(Map<String, String> req);//查询最大流水号
}
