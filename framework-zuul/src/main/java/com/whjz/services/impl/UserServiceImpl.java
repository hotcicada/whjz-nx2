package com.whjz.services.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.whjz.entity.UserParam;
import com.whjz.redis.RedisUtils;
import com.whjz.services.UserService;
import com.whjz.utils.CodeUtil;
import com.whjz.utils.ResultCode;
import com.whjz.utils.ResultUtil;
import com.whjz.utils.StringConstantUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service(value = "userService")
@RefreshScope
public class UserServiceImpl implements UserService {
	
	@Autowired
	private Environment environment;
	
	//所有接口device验证
	public Map<String, Object> checkDevice(UserParam userParam){
		
		String clientSeqNo = userParam.getClientSeqNo();		//客户号
		String deviceId = userParam.getDeviceId();				//设备号
		//在客服号不为空的情况下，判断存入的用户信息
		if(StringUtils.isNotBlank(clientSeqNo) && StringUtils.isNotBlank(deviceId)) {
			String userInfo = RedisUtils.getValue(StringConstantUtil.USER + clientSeqNo);		//用户信息
			//判断用户信息的设备号与请求的设备号是否一致
			if(StringUtils.isNotBlank(userInfo)) {
				UserParam lastUser = JSONObject.parseObject(userInfo, UserParam.class);				//字符转对象
				//比对接口设备号和用户登录的设备号
				if(!deviceId.equals(lastUser.getDeviceId())) {
					return ResultUtil.resultMap(CodeUtil.CODE_3.getName(), CodeUtil.CODE_3.getCode(), null);
				}
			}
		}
		return ResultUtil.resultMap("Token验证成功", ResultCode.RESULT_CODE_SUCCESS.code(), null);
	}
	
	//登录接口token验证
	public Map<String, Object> checkToken(UserParam userParam) {
		Map<String, Object> map = new HashMap<String, Object>();

		log.info("********************请求参数是:{}"+JSON.toJSONString(userParam));
		if (org.apache.commons.lang3.StringUtils.isBlank(userParam.getCustNo())){
			return ResultUtil.resultMap("custNo值为空", CodeUtil.CODE_500.getCode(),null);
		}
		if (org.apache.commons.lang3.StringUtils.isBlank(userParam.getSourceType())){
			return ResultUtil.resultMap("sourceType值为空", CodeUtil.CODE_500.getCode(),null);
		}
		if (org.apache.commons.lang3.StringUtils.isBlank(userParam.getToken())){
			return ResultUtil.resultMap("token值为空", CodeUtil.CODE_500.getCode(),null);
		}
		String tokenKey = null;
		String userKey = null;
		switch (userParam.getSource()){
			case 2://工作站key
				tokenKey = StringConstantUtil.TOKEN;
				userKey = StringConstantUtil.USER;
				break;
			case 3://管理端信息
				tokenKey = StringConstantUtil.MANAGE_TOKEN_ASSIST;
				userKey = StringConstantUtil.MANAGE_USER_ASSIST;
				break;

		}
		String clientSeqNo = userParam.getCustNo();		//客户号
		if(StringUtils.isBlank(clientSeqNo)) {
			return ResultUtil.resultMap(CodeUtil.CODE_2.getName(), CodeUtil.CODE_2.getCode(), map);
		}
		
		String token = RedisUtils.getValue(tokenKey + clientSeqNo);
		String userInfo = RedisUtils.getValue(userKey + clientSeqNo);

		log.info(clientSeqNo+"用户：********************前端传入得 token:{}"+ userParam.getToken());
		log.info(clientSeqNo+"用户：********************登录请求 从Redis中取出的 token:{}"+ token);
		log.info(clientSeqNo+"用户：********************登录请求 从Redis中取出的 userInfo:{}" + userInfo);
		
		//TOKEN认证
		if (StringUtils.isEmpty(token) || StringUtils.isEmpty(userInfo)) {
			log.info(clientSeqNo+"*****Redis中无token信息，已经失效******");
			return ResultUtil.resultMap(CodeUtil.CODE_2.getName(), CodeUtil.CODE_2.getCode(), map);
		}
		
		if(!token.equals(userParam.getToken())) {
			return ResultUtil.resultMap(CodeUtil.CODE_1.getName(), CodeUtil.CODE_1.getCode(), map);
		}
		
		log.info("********************Token验证成功********************" );
		//设置redis的时间
		RedisUtils.setKeyAndCacheTime(tokenKey + clientSeqNo, token, environment.getProperty("redis.maxInactiveIntervalInSeconds",Long.class));
		RedisUtils.setKeyAndCacheTime(userKey + clientSeqNo, userInfo, environment.getProperty("redis.maxInactiveIntervalInSeconds",Long.class));
		
		return ResultUtil.resultMap("Token验证成功", ResultCode.RESULT_CODE_SUCCESS.code(), map);
	}


}
