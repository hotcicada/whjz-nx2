package com.whjz.services.impl;

import com.whjz.dao.MbRequestLogMapper;
import com.whjz.entity.MbRequestLog;
import com.whjz.services.RequestService;
import com.whjz.utils.ResultCode;
import com.whjz.utils.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import java.util.Map;



@Slf4j
@Service
@RefreshScope
public class RequestServiceImpl implements RequestService {


	@Autowired
	@SuppressWarnings("all")
	MbRequestLogMapper mbRequestLogMapper;
	/**
	 * @Desc :添加日志请求流水记录
	 * @Author :gzx    @Date: 2019/12/19 16:44
	 * @Param: @Return:
	 */
	@Override
	public Map addRequestLog(MbRequestLog requestLog) {
		try {
			Integer updateCount = null;
			updateCount = mbRequestLogMapper.insertSelective(requestLog);
			return ResultUtil.resultMap("操作成功", ResultCode.RESULT_CODE_SUCCESS.code(), updateCount);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return ResultUtil.resultMap(ResultCode.RESUTL_CODE_FAILURE.desc(), ResultCode.RESUTL_CODE_FAILURE.code(), null);
	}


	/**
	 * 获取最大后台流水号
	 *
	 * @param req
	 * @return
	 */
	@Override
	public String queryTheMaxBackReqNO(Map<String, String> req) {
		MbRequestLog mbRequestLog = mbRequestLogMapper.selectTheMaxBackReqNo();
		if (mbRequestLog == null){
			return "";
		}
		return mbRequestLog.getBackSeqNo();
	}

}
