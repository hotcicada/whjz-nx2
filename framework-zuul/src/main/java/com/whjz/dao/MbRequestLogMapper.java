package com.whjz.dao;

import com.whjz.entity.MbRequestLog;

import java.util.List;
import java.util.Map;

public interface MbRequestLogMapper {
    int deleteByPrimaryKey(Integer autoId);

    int insert(MbRequestLog record);

    int insertSelective(MbRequestLog record);

    MbRequestLog selectByPrimaryKey(Integer autoId);

    int updateByPrimaryKeySelective(MbRequestLog record);

    int updateByPrimaryKey(MbRequestLog record);

    List<MbRequestLog> selectRequestLogBySeqNo(Map req);
    MbRequestLog selectTheMaxBackReqNo();
}