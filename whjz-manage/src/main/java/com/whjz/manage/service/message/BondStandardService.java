package com.whjz.manage.service.message;

import java.util.Map;
/**
 * author:hj
 * data:2020-10-10
 * remarks:供应商保证金标准接口
 */
public interface BondStandardService {

    Map<String, Object> queryBondStandardList(Map<String, String> paramMap);

    Map<String, Object> insertBondStandard(Map<String, String> paramMap);

    Map<String, Object> updateBondStandard(Map<String, String> paramMap);

    Map<String, Object> deleteBondStandard(Map<String, String> paramMap);
}
