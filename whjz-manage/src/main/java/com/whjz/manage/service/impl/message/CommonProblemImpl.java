package com.whjz.manage.service.impl.message;

import com.whjz.entity.message.CommonProblem;
import com.whjz.manage.dao.message.CommonProblemMapper;
import com.whjz.manage.service.message.CommonProblemService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author:hj
 * date:2020-10-11
 * remarks:常见问题类
 */
@Slf4j
@Service(value ="commonProblem" )
@SuppressWarnings("all")
public  class CommonProblemImpl implements CommonProblemService {

@Autowired
    private CommonProblemMapper commonProblemMapper;

    /**s
     * @param paramMap 添加供常见问题
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> insertCommonProblem(Map<String, String> paramMap) {
        try {
            String date  = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            paramMap.put("updateDate", date);
            boolean flag =commonProblemMapper.insertCommonProblem(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }


    /**
     * @param paramMap 修改常见问题
     * @return 修改成功或失败
     */@Override
    public Map<String, Object> updateCommonProblem(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("updateDate", date);

            boolean flag = commonProblemMapper.updateCommonProblem (paramMap);

            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap 删除常见问题
     * @return 删除成功或失败
     */
    @Override
    public Map<String, Object> deleteCommonProblem(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoId") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoId")))) {
                String autoIds = paramMap.get("autoId");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {

                    commonProblemMapper.deleteCommonProblem(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 查询常见问题
     * @return 查询成功或失败
     */
    @Override
    public Map<String, Object> queryCommonProblemList(Map<String, String> paramMap) {
        try {
            List<CommonProblem> queryCommonProblem =commonProblemMapper.queryCommonProblemList(paramMap);
            int count = commonProblemMapper.queryCommonProblemCount(paramMap);

            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap,queryCommonProblem,count));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}
