package com.whjz.manage.service.impl.message;

import com.whjz.entity.message.Stowage;
import com.whjz.manage.dao.message.StowageMapper;
import com.whjz.manage.service.message.StowageService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author:hj
 * date:2020-10-12
 * remarks:配载单管理
 */
@Slf4j
@Service(value = "stowage")
@SuppressWarnings("all")
public class StowageImpl implements StowageService {

    @Autowired
    private StowageMapper stowageMapper;

    /**
     * @param paramMap 查询配载单管理
     * @return 查询成功或失败
     */
    @Override
    public Map<String, Object> queryStowageList(Map<String, String> paramMap) {
        try {
            List<Stowage> queryStowage = stowageMapper.queryStowageList(paramMap);

            int count = stowageMapper.queryStowageCount(paramMap);

            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, queryStowage, count));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 添加配载单管理
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> insertStowage(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            paramMap.put("updateDate", date);
            boolean flag = stowageMapper.insertStowage(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 修改配载单管理
     * @return 修改成功或失败
     */
    @Override
    public Map<String, Object> updateStowage(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("updateDate", date);

            boolean flag = stowageMapper.updateStowage(paramMap);

            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 删除配载单管理
     * @return 删除成功或失败
     */
    @Override
    public Map<String, Object> deleteStowage(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoId") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoId")))) {
                String autoIds = paramMap.get("autoId");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {

                    stowageMapper.deleteStowage(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

}
