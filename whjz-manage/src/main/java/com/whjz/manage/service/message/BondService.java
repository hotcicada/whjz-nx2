package com.whjz.manage.service.message;

import java.util.Map;

/**
 * author:hj
 * data:2020-10-10
 * remarks:供应商保证金接口
 */
public interface BondService {

    Map<String, Object> queryBondList(Map<String, String> paramMap);

    Map<String, Object> insertBond(Map<String, String> paramMap);

    Map<String, Object> updateBond(Map<String, String> paramMap);

    Map<String, Object> deleteBond(Map<String, String> paramMap);

}
