package com.whjz.manage.service.impl.system;

import com.whjz.manage.dao.system.SysUserOnlineMapper;
import com.whjz.entity.system.SysUserOnline;
import com.whjz.manage.service.system.SysUserOnlineService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-22
 * remarks：机构相关接口
 */
@Slf4j
@Service(value = "sysUserOnlineService")
@SuppressWarnings("all")
public class SysUserOnlineServiceImpl implements SysUserOnlineService {

    @Autowired
    private SysUserOnlineMapper sysUserOnlineMapper;

    /**
     *
     * @param paramMap  查询机构条件
     * @return  查询成功或失败
     */
    @Override
    public Map<String, Object> queryUserOnlineList(Map<String, String> paramMap) {
        try {
            List<SysUserOnline> sysUserList = sysUserOnlineMapper.queryUserOnlineList(paramMap);
            int count = sysUserOnlineMapper.queryUserOnlineCount(paramMap);
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, sysUserList, count));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap  机构相关信息
     * @return  添加成功或失败
     */
    @Override
    public Map<String, Object> insertUserOnline(Map<String, String> paramMap) {
        try {
//            paramMap.put("userId", "sysuser" + System.currentTimeMillis() + (new Random().nextInt(99999 - 10000) + 10000 + 1));
            paramMap.put("states", StringConstantUtil.USERONLINESTATESONLINE);
            boolean flag = sysUserOnlineMapper.insertUserOnline(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap  机构相关信息
     * @return  修改成功或失败
     */
    @Override
    public Map<String, Object> updateUserOnline(Map<String, String> paramMap) {
        try {
            String date  = DateUtils.getCurDateTime();
            paramMap.put("updateDate", date);
            boolean flag = sysUserOnlineMapper.modifyUserOnline(paramMap);
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> deleteUserOnline(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoIds") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoIds")))) {
                String autoIds = paramMap.get("autoIds");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {
                    sysUserOnlineMapper.deleteUserOnline(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}
