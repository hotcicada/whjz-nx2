package com.whjz.manage.service.message;

import java.util.Map;

/**
 * author:hj
 * date:2020-10-10
 * remarks:供应商接口
 */
public interface SupplierService {

    Map<String, Object> querySupplierList(Map<String, String> paramMap);

    Map<String, Object> insertSupplier(Map<String, String> paramMap);

    Map<String, Object> updateSupplier(Map<String, String> paramMap);

    Map<String, Object> deleteSupplier(Map<String, String> paramMap);

}
