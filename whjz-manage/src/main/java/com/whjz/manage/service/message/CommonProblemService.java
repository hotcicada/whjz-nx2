package com.whjz.manage.service.message;

import java.util.Map;
/**
 * author:hj
 * data:2020-10-11
 * remarks:常见问题接口
 */
public interface CommonProblemService {


    Map<String, Object> insertCommonProblem(Map<String, String> paramMap);

    Map<String, Object> updateCommonProblem(Map<String, String> paramMap);

    Map<String, Object> deleteCommonProblem(Map<String, String> paramMap);

    Map<String, Object> queryCommonProblemList(Map<String, String> paramMap);

}
