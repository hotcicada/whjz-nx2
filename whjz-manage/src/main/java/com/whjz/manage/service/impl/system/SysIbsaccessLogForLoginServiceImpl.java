package com.whjz.manage.service.impl.system;

import com.whjz.manage.dao.system.SysIbsaccessLogForLoginMapper;
import com.whjz.entity.system.SysIbsaccessLogForLogin;
import com.whjz.manage.service.system.SysIbsaccessLogForLoginService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-22
 * remarks：登录日志相关接口
 */
@Slf4j
@Service(value = "sysIbsaccessLogForLoginService")
@SuppressWarnings("all")
public class SysIbsaccessLogForLoginServiceImpl implements SysIbsaccessLogForLoginService {

    @Autowired
    private SysIbsaccessLogForLoginMapper sysIbsaccessLogForLoginMapper;

    /**
     *
     * @param paramMap  查询登录日志条件
     * @return  查询成功或失败
     */
    @Override
    public Map<String, Object> queryIbsaccessLogForLoginList(Map<String, String> paramMap) {
        try {
            List<SysIbsaccessLogForLogin> sysUserList = sysIbsaccessLogForLoginMapper.queryIbsaccessLogForLoginList(paramMap);
            int count = sysIbsaccessLogForLoginMapper.queryIbsaccessLogForLoginCount(paramMap);
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, sysUserList, count));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap  登录日志相关信息
     * @return  添加成功或失败
     */
    @Override
    public Map<String, Object> insertIbsaccessLogForLogin(Map<String, String> paramMap) {
        try {
//            paramMap.put("userId", "sysuser" + System.currentTimeMillis() + (new Random().nextInt(99999 - 10000) + 10000 + 1));
            String date  = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            paramMap.put("updateDate", date);
            boolean flag = sysIbsaccessLogForLoginMapper.insertIbsaccessLogForLogin(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap  登录日志相关信息
     * @return  修改成功或失败
     */
    @Override
    public Map<String, Object> updateIbsaccessLogForLogin(Map<String, String> paramMap) {
        try {
            String date  = DateUtils.getCurDateTime();
            paramMap.put("updateDate", date);
            boolean flag = sysIbsaccessLogForLoginMapper.modifyIbsaccessLogForLogin(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> deleteIbsaccessLogForLogin(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoIds") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoIds")))) {
                String autoIds = paramMap.get("autoIds");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {
                    sysIbsaccessLogForLoginMapper.deleteIbsaccessLogForLogin(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}
