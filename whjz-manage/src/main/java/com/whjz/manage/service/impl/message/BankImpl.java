package com.whjz.manage.service.impl.message;

import com.whjz.entity.message.Bank;
import com.whjz.manage.dao.message.BankMapper;
import com.whjz.manage.service.message.BankService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author:hj
 * date:2020-10-12
 * remarks:银行管理类
 */
@Slf4j
@Service(value ="bank" )
@SuppressWarnings("all")
public class BankImpl implements BankService {

    @Autowired
    private BankMapper bankMapper;

    /**
     *
     * @param paramMap 查询银行管理
     * @return 查询成功或失败
     */
    @Override
    public Map<String, Object> queryBankList(Map<String, String> paramMap) {
        try {
            List<Bank> queryBank = bankMapper.queryBankList(paramMap);
            int count = bankMapper.queryBankCount(paramMap);

            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, queryBank, count));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap 添加银行管理
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> insertBank(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            paramMap.put("updateDate", date);
            boolean flag = bankMapper.insertBank(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 修改银行管理
     * @return 修改成功或失败
     */
    @Override
    public Map<String, Object> updateBank(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("updateDate", date);

            boolean flag = bankMapper.updateBank(paramMap);

            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap 删除银行管理
     * @return 删除成功或失败
     */
    @Override
    public Map<String, Object> deleteBank(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoId") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoId")))) {
                String autoIds = paramMap.get("autoId");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {

                    bankMapper.deleteBank(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }


}
