package com.whjz.manage.service.impl.message;

import com.alibaba.fastjson.JSON;
import com.whjz.entity.message.Contract;
import com.whjz.entity.system.SysUser;
import com.whjz.manage.dao.message.ContractMapper;
import com.whjz.manage.service.message.ContractService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：用户相关接口
 */
@Slf4j
@Service(value = "contractService")
@SuppressWarnings("all")
public class ContractServiceImpl implements ContractService {

    @Autowired
    private ContractMapper contractMapper;

    /**
     * @param paramMap 查询用户条件
     * @return 查询成功或失败
     */
    @Override
    public Map<String, Object> queryContractList(Map<String, String> paramMap) {
        try {
            List<Contract> contractList = contractMapper.queryContractList(paramMap);
            int contractCount = contractMapper.queryContractCount(paramMap);
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, contractList, contractCount));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 用户相关信息
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> insertContract(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            boolean flag = contractMapper.insertContract(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 用户相关信息
     * @return 修改成功或失败
     */
    @Override
    public Map<String, Object> updateContract(Map<String, String> paramMap) {
        try {
            boolean flag = contractMapper.modifyContract(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> deleteContract(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoIds") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoIds")))) {
                String autoIds = paramMap.get("autoIds");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {
                    contractMapper.deleteContract(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}
