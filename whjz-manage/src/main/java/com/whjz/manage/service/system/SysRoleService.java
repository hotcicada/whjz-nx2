package com.whjz.manage.service.system;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：字典类别相关接口
 */

public interface SysRoleService {

    Map<String, Object> queryRoleList(Map<String, String> paramMap);

    Map<String, Object> insertRole(Map<String, String> paramMap);

    Map<String, Object> updateRole(Map<String, String> paramMap);

    Map<String, Object> deleteRole(Map<String, String> paramMap);
}
