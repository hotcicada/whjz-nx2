package com.whjz.manage.service.system;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-24
 * remarks：日志相关接口
 */

public interface SysRequestLogService {

    Map<String, Object> queryRequestLogList(Map<String, String> paramMap);

    Map<String, Object> insertRequestLog(Map<String, String> paramMap);

    Map<String, Object> updateRequestLog(Map<String, String> paramMap);

    Map<String, Object> deleteRequestLog(Map<String, String> paramMap);
}
