package com.whjz.manage.service.system;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-21
 * remarks：字典相关接口
 */

public interface SysDictDataService {

    Map<String, Object> queryDictDataList(Map<String, String> paramMap);

    Map<String, Object> insertDictData(Map<String, String> paramMap);

    Map<String, Object> updateDictData(Map<String, String> paramMap);

    Map<String, Object> deleteDictData(Map<String, String> paramMap);
}
