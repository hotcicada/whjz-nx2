package com.whjz.manage.service.impl.message;

import com.whjz.entity.message.StorageRoom;
import com.whjz.manage.dao.message.StorageRoomMapper;
import com.whjz.manage.service.message.StorageRoomService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author:hj
 * date:2020-10-13
 * remarks:储货间
 */
@Slf4j
@Service(value ="storageRoom" )
@SuppressWarnings("all")
public class StorageRoomImpl implements StorageRoomService {
    @Autowired
    private StorageRoomMapper storageRoomMapper;

    /**
     * @param paramMap 查询储货间
     * @return 查询成功或失败
     */
    @Override
    public Map<String, Object> queryStorageRoomList(Map<String, String> paramMap) {
        try {
            List<StorageRoom> queryStorageRoom = storageRoomMapper.queryStorageRoomList(paramMap);
            int count = storageRoomMapper.queryStorageRoomCount(paramMap);

            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, queryStorageRoom, count));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 添加储物间
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> insertStorageRoom(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            paramMap.put("updateDate", date);
            boolean flag = storageRoomMapper.insertStorageRoom(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 修改储物间
     * @return 修改成功或失败
     */
    @Override
    public Map<String, Object> updateStorageRoom(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();

            paramMap.put("updateDate", date);

            boolean flag = storageRoomMapper.updateStorageRoom(paramMap);

            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);

    }

    /**
     *
     * @param paramMap 删除储物间
     *
     * @return 删除成功或失败
     */
    @Override
    public Map<String, Object> deleteStorageRoom(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoId") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoId")))) {
                String autoIds = paramMap.get("autoId");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {

                    storageRoomMapper.deleteStorageRoom(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}

