package com.whjz.manage.service.message;

import java.util.Map;

/**
 * author:hj
 * date:2020-10-12
 * remarks:银行管理接口
 */
public interface BankService {

    Map<String, Object> queryBankList(Map<String, String> paramMap);

    Map<String, Object> insertBank(Map<String, String> paramMap);

    Map<String, Object> updateBank(Map<String, String> paramMap);

    Map<String, Object> deleteBank(Map<String, String> paramMap);



}
