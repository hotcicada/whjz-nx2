package com.whjz.manage.service.impl.message;

import com.whjz.entity.message.Picture;
import com.whjz.manage.dao.message.PictureMapper;
import com.whjz.manage.service.message.PictureService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-10-11
 * remarks：图片相关接口
 */
@Slf4j
@Service(value = "PictureService")
@SuppressWarnings("all")
public class PictureServiceImpl implements PictureService {

    @Autowired
    private PictureMapper pictureMapper;

    /**
     * @param paramMap 查询用户条件
     * @return 查询成功或失败
     */
    @Override
    public Map<String, Object> queryPictureList(Map<String, String> paramMap) {
        try {
            List<Picture> pictureList = pictureMapper.queryPictureList(paramMap);
            int pictureCount = pictureMapper.queryPictureCount(paramMap);
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, pictureList, pictureCount));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 用户相关信息
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> insertPicture(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            boolean flag = pictureMapper.insertPicture(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 用户相关信息
     * @return 修改成功或失败
     */
    @Override
    public Map<String, Object> updatePicture(Map<String, String> paramMap) {
        try {
            boolean flag = pictureMapper.modifyPicture(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> deletePicture(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoIds") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoIds")))) {
                String autoIds = paramMap.get("autoIds");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {
                    pictureMapper.deletePicture(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}
