package com.whjz.manage.service.message;

import java.util.Map;

/**
 * author: fln
 * date: 2020-10-11
 * remarks：产品相关接口
 */

public interface ProductService {

    Map<String, Object> queryProductList(Map<String, String> paramMap);

    Map<String, Object> insertProduct(Map<String, String> paramMap);

    Map<String, Object> deleteProduct(Map<String, String> paramMap);

    Map<String, Object> updateProduct(Map<String, String> paramMap);
}
