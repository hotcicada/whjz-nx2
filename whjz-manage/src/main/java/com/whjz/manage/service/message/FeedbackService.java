package com.whjz.manage.service.message;

import java.util.Map;

public interface FeedbackService {

    Map<String, Object> insertFeedback(Map<String, String> paramMap);

    Map<String, Object> updateFeedback(Map<String, String> paramMap);

    Map<String, Object> deleteFeedback(Map<String, String> paramMap);

    Map<String, Object> queryFeedbackList(Map<String, String> paramMap);
}
