package com.whjz.manage.service.message;

import java.util.Map;

/**
 * author: fln
 * date: 2020-10-10
 * remarks：订单相关接口
 */

public interface ShoppingCartService {

    Map<String, Object> queryShoppingCartList(Map<String, String> paramMap);

    Map<String, Object> insertShoppingCart(Map<String, String> paramMap);

    Map<String, Object> deleteShoppingCart(Map<String, String> paramMap);

    Map<String, Object> updateShoppingCart(Map<String, String> paramMap);
}
