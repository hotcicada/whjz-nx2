package com.whjz.manage.service.impl.message;

import com.whjz.entity.message.BondStandard;
import com.whjz.manage.dao.message.BondStandardMapper;
import com.whjz.manage.service.message.BondStandardService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 *  author: hj
 *  date: 2020-10-10
 *  remarks：供应商保证金标准类
 */
@Slf4j
@Service(value ="BondStandard" )
@SuppressWarnings("all")
public  class BondStandardImpl implements BondStandardService {
@Autowired
private BondStandardMapper bondStandardMapper;

    /**
     * @param paramMap 查询供应商保证金标准
     * @return 查询成功或失败
     */
    @Override
    public Map<String, Object> queryBondStandardList(Map<String, String> paramMap) {
        try {
            List<BondStandard> queryBondStandardList =bondStandardMapper.queryBondStandardList(paramMap);
            int count = bondStandardMapper.queryBondStandardCount(paramMap);

            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap,queryBondStandardList,count));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**s
     * @param paramMap 添加供应商保证金标准
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> insertBondStandard(Map<String, String> paramMap) {
        try {
            String date  = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            paramMap.put("updateDate", date);
            boolean flag =bondStandardMapper.insertBondStandard(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 供应商保证金标准
     * @return 修改成功或失败
     */
    @Override
    public Map<String, Object> updateBondStandard(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("updateDate", date);

            boolean flag = bondStandardMapper.updateBondStandard (paramMap);

            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }



    /**
     *
     * @param paramMap 供应商保证金标准
     * @return 删除成功或失败
     */
    @Override
    public Map<String, Object> deleteBondStandard(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoId") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoId")))) {
                String autoIds = paramMap.get("autoId");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {

                    bondStandardMapper.deleteBondStandard(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);

    }
}

