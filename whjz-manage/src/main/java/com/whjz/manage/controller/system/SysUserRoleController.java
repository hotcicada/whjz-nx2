package com.whjz.manage.controller.system;

import com.whjz.manage.service.system.SysUserRoleService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-23
 * remarks：用户角色相关接口
 */

@Api(tags = {"用户角色管理"})
@RestController
@Slf4j
@RequestMapping("sysUserRole")
public class SysUserRoleController extends AbstractControllerImpl {

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @PostMapping("/queryUserRoleList")
    @ApiOperation(value = "查询用户角色列表", notes = "根据查询条件返回用户角色列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "pageNum",value = "当前页数",paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize",value = "查询记录数",paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage",value = "是否分页",paramType = "query", required = true),
    })
    public Map<String,Object> queryUserRoleList(@RequestBody Map<String,String> paramMap) {
        return sysUserRoleService.queryUserRoleList(paramMap);
    }

    @PostMapping("/insertUserRole")
    @ApiOperation(value = "添加用户角色", notes = "添加用户角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId",value = "用户ID",paramType = "query", required = true),
            @ApiImplicitParam(name = "roleCode",value = "角色ID",paramType = "query", required = true),
            @ApiImplicitParam(name = "operator",value = "操作人",paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate",value = "创建时间",paramType = "query"),
            @ApiImplicitParam(name = "updateDate",value = "更新时间",paramType = "query", required = true),
            @ApiImplicitParam(name = "state",value = "状态标识(1--启用  2--停用)",paramType = "query"),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> insertUserRole(@RequestBody Map<String,String> paramMap) {
        return sysUserRoleService.insertUserRole(paramMap);
    }

    @PostMapping("/updateUserRole")
    @ApiOperation(value = "修改用户角色", notes = "修改用户角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId",value = "用户角色编号",paramType = "query", required = true),
            @ApiImplicitParam(name = "roleCode",value = "用户角色名称",paramType = "query", required = true),
            @ApiImplicitParam(name = "operator",value = "上级用户角色ID",paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate",value = "用户角色简称",paramType = "query"),
            @ApiImplicitParam(name = "updateDate",value = "用户角色级别(0-总公司 1-分公司 3.部门)",paramType = "query", required = true),
            @ApiImplicitParam(name = "state",value = "状态标识(1--启用  2--停用)",paramType = "query"),
            @ApiImplicitParam(name = "UserRoleType",value = "用户角色类型",paramType = "query"),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> updateUserRole(@RequestBody Map<String,String> paramMap) {
        return sysUserRoleService.updateUserRole(paramMap);
    }

    @PostMapping("/deleteUserRole")
    @ApiOperation(value = "删除用户角色", notes = "删除用户角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds",value = "字符串拼接ID",paramType = "query", required = true),
    })
    public Map<String,Object> deleteUserRole(@RequestBody Map<String,String> paramMap) {
        return sysUserRoleService.deleteUserRole(paramMap);
    }
}
