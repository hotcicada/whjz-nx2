package com.whjz.manage.controller.system;

import com.whjz.manage.service.system.SysRoleService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：角色相关接口
 */

@Api(tags = {"角色管理"})
@RestController
@Slf4j
@RequestMapping("sysRole")
public class SysRoleController extends AbstractControllerImpl {

    @Autowired
    private SysRoleService sysRoleService;

    @PostMapping("/queryRoleList")
    @ApiOperation(value = "查询角色列表", notes = "根据查询条件返回角色列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "deptId",value = "部门 筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "roleState",value = "角色状态 1-启用,2-停用 筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "roleType",value = "角色类型 1-管理端,2-客户端 筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "startDate",value = "开始时间 筛选条件 (实际查询的是创建时间满足开始时间到结束时间的数据)",paramType = "query"),
            @ApiImplicitParam(name = "endDate",value = "结束时间 筛选条件 (实际查询的是创建时间满足开始时间到结束时间的数据)",paramType = "query"),
            @ApiImplicitParam(name = "pageNum",value = "当前页数",paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize",value = "查询记录数",paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage",value = "是否分页",paramType = "query", required = true),
    })
    public Map<String,Object> queryRoleList(@RequestBody Map<String,String> paramMap) {
        return sysRoleService.queryRoleList(paramMap);
    }

    @PostMapping("/insertRole")
    @ApiOperation(value = "添加角色", notes = "添加角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deptId",value = "创建机构",paramType = "query", required = true),
            @ApiImplicitParam(name = "roleCode",value = "角色编号（岗位编号 用户自己输入）",paramType = "query", required = true),
            @ApiImplicitParam(name = "roleName",value = "角色名称",paramType = "query", required = true),
            @ApiImplicitParam(name = "roleDesc",value = "角色描述",paramType = "query"),
            @ApiImplicitParam(name = "roleState",value = "角色状态,1-启用,2-停用",paramType = "query"),
            @ApiImplicitParam(name = "roleType",value = "角色类型",paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser",value = "创建人",paramType = "query", required = true),
            @ApiImplicitParam(name = "updateUser",value = "操作人",paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> insertRole(@RequestBody Map<String,String> paramMap) {
        return sysRoleService.insertRole(paramMap);
    }

    @PostMapping("/updateRole")
    @ApiOperation(value = "修改角色", notes = "修改角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "roleCode",value = "角色编号（岗位编号 用户自己输入）",paramType = "query", required = true),
            @ApiImplicitParam(name = "roleName",value = "角色名称",paramType = "query", required = true),
            @ApiImplicitParam(name = "roleDesc",value = "角色描述",paramType = "query"),
            @ApiImplicitParam(name = "roleState",value = "角色状态,1-启用,2-停用",paramType = "query"),
            @ApiImplicitParam(name = "roleType",value = "角色类型",paramType = "query", required = true),
            @ApiImplicitParam(name = "updateUser",value = "操作人",paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> updateRole(@RequestBody Map<String,String> paramMap) {
        return sysRoleService.updateRole(paramMap);
    }

    @PostMapping("/deleteRole")
    @ApiOperation(value = "删除角色", notes = "删除角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds",value = "字符串拼接ID",paramType = "query", required = true),
    })
    public Map<String,Object> deleteRole(@RequestBody Map<String,String> paramMap) {
        return sysRoleService.deleteRole(paramMap);
    }
}
