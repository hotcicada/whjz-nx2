package com.whjz.manage.controller.message;

import com.whjz.manage.service.message.ShoppingCartService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-10-12
 * remarks：购物车相关接口
 */
@Api(tags = {"购物车管理"})
@RestController
@Slf4j
@RequestMapping("shoppingCart")
public class ShoppingCartController extends AbstractControllerImpl {

    @Autowired
    private ShoppingCartService shoppingCartService;

    @PostMapping("/queryShoppingCartList")
    @ApiOperation(value = "查询购物车列表", notes = "根据查询条件返回购物车列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "shoppingCartCode", value = "购物车编号 筛选条件", paramType = "query"),
            @ApiImplicitParam(name = "productFirstTypeCode", value = "商品一级分类编号 筛选条件", paramType = "query"),
            @ApiImplicitParam(name = "productSecondTypeCode", value = "商品二级分类编号 筛选条件", paramType = "query"),
            @ApiImplicitParam(name = "productThirdTypeCode", value = "商品三级分类编号 筛选条件", paramType = "query"),
            @ApiImplicitParam(name = "productCode", value = "商品编号", paramType = "query"),
            @ApiImplicitParam(name = "productName", value = "商品名称", paramType = "query"),
            @ApiImplicitParam(name = "productSpecifica", value = "商品规格", paramType = "query"),
            @ApiImplicitParam(name = "platformPrice", value = "平台价格", paramType = "query"),
            @ApiImplicitParam(name = "supermarketPrice", value = "超市价格", paramType = "query"),
            @ApiImplicitParam(name = "copiesNumber", value = "份数", paramType = "query"),
            @ApiImplicitParam(name = "ShoppingCartState", value = "状态", paramType = "query"),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query"),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query"),
            @ApiImplicitParam(name = "pageNum", value = "当前页数", paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize", value = "查询记录数", paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage", value = "是否分页", paramType = "query", required = true),
    })
    public Map<String, Object> queryShoppingCartList(@RequestBody Map<String, String> paramMap) {
        return shoppingCartService.queryShoppingCartList(paramMap);
    }

    @PostMapping("/insertShoppingCart")
    @ApiOperation(value = "添加购物车", notes = "添加购物车")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "shoppingCartCode", value = "购物车编号", paramType = "query"),
            @ApiImplicitParam(name = "productFirstTypeCode", value = "商品一级分类编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "productFirstTypeName", value = "商品一级分类名称", paramType = "query"),
            @ApiImplicitParam(name = "productSecondTypeCode", value = "商品二级分类编号", paramType = "query"),
            @ApiImplicitParam(name = "productSecondTypeName", value = "商品二级分类名称", paramType = "query"),
            @ApiImplicitParam(name = "productThirdTypeCode", value = "商品三级分类编号", paramType = "query"),
            @ApiImplicitParam(name = "productThirdTypeName", value = "商品三级分类名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "productCode", value = "商品编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "productName", value = "商品名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "productSpecifica", value = "商品规格", paramType = "query"),
            @ApiImplicitParam(name = "platformPrice", value = "平台价格", paramType = "query"),
            @ApiImplicitParam(name = "supermarketPrice", value = "超市价格", paramType = "query"),
            @ApiImplicitParam(name = "copiesNumber", value = "份数", paramType = "query"),
            @ApiImplicitParam(name = "shoppingCartState", value = "状态 1-下单成功2-商家备货中 3-物流配送中 4-自提点待提货 5-已提货6-待评价 ", paramType = "query"),
            @ApiImplicitParam(name = "sellerCode", value = "卖家编号", paramType = "query"),
            @ApiImplicitParam(name = "sellerName", value = "卖家名称", paramType = "query"),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query"),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query"),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
    })
    public Map<String, Object> insertShoppingCart(@RequestBody Map<String, String> paramMap) {
        return shoppingCartService.insertShoppingCart(paramMap);
    }

    @PostMapping("/updateShoppingCart")
    @ApiOperation(value = "修改购物车", notes = "修改购物车")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId", value = "自增ID", paramType = "query"),
            @ApiImplicitParam(name = "ShoppingCartCode", value = "购物车编号", paramType = "query"),
            @ApiImplicitParam(name = "productFirstTypeCode", value = "商品一级分类编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "productFirstTypeName", value = "商品一级分类名称", paramType = "query"),
            @ApiImplicitParam(name = "productSecondTypeCode", value = "商品二级分类编号", paramType = "query"),
            @ApiImplicitParam(name = "productAecondTypeName", value = "商品二级分类名称", paramType = "query"),
            @ApiImplicitParam(name = "productThirdTypeCode", value = "商品三级分类编号", paramType = "query"),
            @ApiImplicitParam(name = "productThirdTypeName", value = "商品三级分类名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "productCode", value = "商品编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "productName", value = "商品名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "productSpecifica", value = "商品规格", paramType = "query"),
            @ApiImplicitParam(name = "platformPrice", value = "平台价格", paramType = "query"),
            @ApiImplicitParam(name = "supermarketPrice", value = "超市价格", paramType = "query"),
            @ApiImplicitParam(name = "copiesNumber", value = "份数", paramType = "query"),
            @ApiImplicitParam(name = "ShoppingCartState", value = "状态 1-下单成功2-商家备货中 3-物流配送中 4-自提点待提货 5-已提货6-待评价 ", paramType = "query"),
            @ApiImplicitParam(name = "sellerCode", value = "卖家编号", paramType = "query"),
            @ApiImplicitParam(name = "sellerName", value = "卖家名称", paramType = "query"),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query"),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query"),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
    })
    public Map<String, Object> updateShoppingCart(@RequestBody Map<String, String> paramMap) {
        return shoppingCartService.updateShoppingCart(paramMap);
    }

    @PostMapping("/deleteShoppingCart")
    @ApiOperation(value = "删除购物车", notes = "删除购物车")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds", value = "字符串拼接ID", paramType = "query", required = true),
    })
    public Map<String, Object> deleteShoppingCart(@RequestBody Map<String, String> paramMap) {
        return shoppingCartService.deleteShoppingCart(paramMap);
    }
}
