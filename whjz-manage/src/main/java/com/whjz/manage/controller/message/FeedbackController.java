package com.whjz.manage.controller.message;

import com.whjz.manage.service.message.FeedbackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author:hj
 * date:2020-10-11
 * remarks:常见问题类
 */
@Api(tags = "意见反馈类")
@Slf4j
@RestController
@RequestMapping("feedback")
public class FeedbackController {

    @Autowired
    @SuppressWarnings("all")
    private FeedbackService feedbackService;


    @PostMapping("/queryFeedbackList")
    @ApiOperation(value = "查询意见反馈列表", notes = "根据条件查询意见反馈返回常见意见反馈列表")

    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "content", value = "意见内容", paramType = "query", required = true),

            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "state", value = "状态", paramType = "query", required = true),

            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> queryCommonProblemList(@RequestBody Map<String, String> paramMap) {

        return feedbackService.queryFeedbackList(paramMap);
    }

    @PostMapping("/insertFeedback")
    @ApiOperation(value = "添加意见反馈列表", notes = "添加意见反馈列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "content", value = "意见内容", paramType = "query", required = true),

            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "state", value = "状态", paramType = "query", required = true),

            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> insertFeedback(@RequestBody Map<String, String> ParamMap) {
        return feedbackService.insertFeedback(ParamMap);
    }
    @PostMapping("/updateFeedback")
    @ApiOperation(value = "修改意见反馈列表", notes = "修改意见反馈列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "content", value = "意见内容", paramType = "query", required = true),

            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "state", value = "状态", paramType = "query", required = true),

            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> updateFeedback(@RequestBody Map<String, String> ParamMap) {
        return feedbackService.updateFeedback(ParamMap);
    }
    @PostMapping("/deleteFeedback")
    @ApiOperation(value = "删除意见反馈列表", notes = "删除意见反馈列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId", value = "字符串拼接ID", paramType = "query", required = true),
    })
    public Map<String, Object> deleteFeedback(@RequestBody Map<String, String> paramMap) {
        return feedbackService.deleteFeedback(paramMap);
    }
}
