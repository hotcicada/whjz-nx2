package com.whjz.manage.controller.message;

import com.whjz.manage.service.message.ContractService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：合同相关接口
 */
@Api(tags = {"合同管理"})
@RestController
@Slf4j
@RequestMapping("contract")
public class ContractController extends AbstractControllerImpl {

    @Autowired
    private ContractService contractService;

    @PostMapping("/queryContractList")
    @ApiOperation(value = "查询合同列表", notes = "根据查询条件返回合同列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "contractCode", value = "合同编号 筛选条件", paramType = "query"),
            @ApiImplicitParam(name = "contractName", value = "合同名称 筛选条件", paramType = "query"),
            @ApiImplicitParam(name = "contractType", value = "合同类型 筛选条件", paramType = "query"),
            @ApiImplicitParam(name = "contractFirstParty", value = "合同甲方 筛选条件", paramType = "query"),
            @ApiImplicitParam(name = "contractSecondParty", value = "合同乙方", paramType = "query"),
            @ApiImplicitParam(name = "pageNum", value = "当前页数", paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize", value = "查询记录数", paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage", value = "是否分页", paramType = "query", required = true),
    })
    public Map<String, Object> queryContractList(@RequestBody Map<String, String> paramMap) {
        return contractService.queryContractList(paramMap);
    }

    @PostMapping("/insertContract")
    @ApiOperation(value = "添加合同", notes = "添加合同")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "contractCode", value = "合同编号", paramType = "query"),
            @ApiImplicitParam(name = "contractName", value = "合同名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "contractType", value = "合同类型1-供应商合同 2-物流合同 3-驿站合同", paramType = "query"),
            @ApiImplicitParam(name = "contractFirstParty", value = "合同甲方", paramType = "query"),
            @ApiImplicitParam(name = "contractSecondParty", value = "合同乙方", paramType = "query"),
            @ApiImplicitParam(name = "contractDesc", value = "合同概况", paramType = "query"),
            @ApiImplicitParam(name = "dueDate", value = "合同到期日", paramType = "query", required = true),
            @ApiImplicitParam(name = "signDate", value = "合同签订日期", paramType = "query", required = true),
            @ApiImplicitParam(name = "contractState", value = "合同期限1、正常2、合同已超期", paramType = "query", required = true),
            @ApiImplicitParam(name = "signPlace", value = "签订地点", paramType = "query"),
            @ApiImplicitParam(name = "contractFile", value = "合同文件", paramType = "query"),
            @ApiImplicitParam(name = "createUser", value = "合同录入人", paramType = "query"),
            @ApiImplicitParam(name = "createDate", value = "合同创建时间", paramType = "query"),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext6", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext7", value = "扩展字段5", paramType = "query"),
    })
    public Map<String, Object> insertContract(@RequestBody Map<String, String> paramMap) {
        return contractService.insertContract(paramMap);
    }

    @PostMapping("/updateContract")
    @ApiOperation(value = "修改合同", notes = "修改合同")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId", value = "自增ID", paramType = "query"),
            @ApiImplicitParam(name = "contractCode", value = "合同编号", paramType = "query"),
            @ApiImplicitParam(name = "contractName", value = "合同名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "contractType", value = "合同类型1-供应商合同 2-物流合同 3-驿站合同", paramType = "query"),
            @ApiImplicitParam(name = "contractFirstParty", value = "合同甲方", paramType = "query"),
            @ApiImplicitParam(name = "contractSecondParty", value = "合同乙方", paramType = "query"),
            @ApiImplicitParam(name = "contractDesc", value = "合同概况", paramType = "query"),
            @ApiImplicitParam(name = "dueDate", value = "合同到期日", paramType = "query", required = true),
            @ApiImplicitParam(name = "signDate", value = "合同签订日期", paramType = "query", required = true),
            @ApiImplicitParam(name = "contractState", value = "合同期限1、正常2、合同已超期", paramType = "query", required = true),
            @ApiImplicitParam(name = "signPlace", value = "签订地点", paramType = "query"),
            @ApiImplicitParam(name = "contractFile", value = "合同文件", paramType = "query"),
            @ApiImplicitParam(name = "createUser", value = "合同录入人", paramType = "query"),
            @ApiImplicitParam(name = "createDate", value = "合同创建时间", paramType = "query"),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext6", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext7", value = "扩展字段5", paramType = "query"),
    })
    public Map<String, Object> updateContract(@RequestBody Map<String, String> paramMap) {
        return contractService.updateContract(paramMap);
    }

    @PostMapping("/deleteContract")
    @ApiOperation(value = "删除合同", notes = "删除合同")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds", value = "字符串拼接ID", paramType = "query", required = true),
    })
    public Map<String, Object> deleteContract(@RequestBody Map<String, String> paramMap) {
        return contractService.deleteContract(paramMap);
    }
}
