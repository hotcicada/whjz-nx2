package com.whjz.manage.controller.message;

import com.whjz.manage.service.impl.message.SupplierContactsImpl;
import com.whjz.manage.service.message.SupplierContactsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author:hj
 * date:2020-10-6
 * remarks:供应商联系人管理相关类
 */
@Api (tags = "供应商联系人管理")
@Slf4j
@RestController
@RequestMapping("SupplierContacts")
public class SupplierContactsController extends SupplierContactsImpl {


    @Autowired
    @SuppressWarnings("all")
    private SupplierContactsService supplierContactsService;

    @PostMapping("/querySupplierContactsList")
    @ApiOperation(value = "查询供应商联系人列表", notes = "根据条件查询供应商联系人返回供应商联系人列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierCode", value = "供应商编码", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierName", value = "供应商联系人", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierTel", value = "联系人电话", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierWeChat", value = "联系人微信", paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
    })
    public Map<String, Object> querySupplierContactsList(@RequestBody Map<String, String> paramMap) {

        return supplierContactsService.querySupplierContactsList(paramMap);
    }

    @PostMapping("/insertSupplierContacts")
    @ApiOperation(value = "添加供应商联系人列表", notes = "添加供应商联系人列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierCode", value = "供应商编码", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierName", value = "供应商联系人", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierTel", value = "联系人电话", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierWeChat", value = "联系人微信", paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),})
    public Map<String, Object> insertSupplierContacts(@RequestBody Map<String, String> ParamMap) {
        return supplierContactsService.insertSupplierContacts(ParamMap);
    }

    @PostMapping("/updateSupplierContacts")
    @ApiOperation(value = "修改供应商联系人列表", notes = "修改供应商联系人列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierCode", value = "供应商编码", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierName", value = "供应商联系人", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierTel", value = "联系人电话", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierWeChat", value = "联系人微信", paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),})
    public Map<String, Object> updateSupplierContacts(@RequestBody Map<String, String> ParamMap) {
        return supplierContactsService.updateSupplierContacts(ParamMap);
    }
    @PostMapping("/deleteSupplierContacts")
    @ApiOperation(value = "删除供应商联系人列表", notes = "删除供应商联系人列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId",value = "字符串拼接ID",paramType = "query", required = true),
    })
    public Map<String,Object> deleteSupplierContacts(@RequestBody Map<String,String> paramMap) {
        return supplierContactsService.deleteSupplierContacts(paramMap);
    }
}


