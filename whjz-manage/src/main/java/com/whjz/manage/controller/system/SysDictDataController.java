package com.whjz.manage.controller.system;

import com.whjz.manage.service.system.SysDictDataService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：字典相关接口
 */

@Api(tags = {"字典管理"})
@RestController
@Slf4j
@RequestMapping("sysDictData")
public class SysDictDataController extends AbstractControllerImpl {

    @Autowired
    private SysDictDataService sysDictDataService;

    @PostMapping("/queryDictDataList")
    @ApiOperation(value = "查询字典列表", notes = "根据查询条件返回字典列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "dictTypeId",value = "字典类型id 筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "dictValue",value = "字典值 筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "state",value = "状态 1-启用，2--停用 筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "pageNum",value = "当前页数",paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize",value = "查询记录数",paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage",value = "是否分页",paramType = "query", required = true),
    })
    public Map<String,Object> queryDictDataList(@RequestBody Map<String,String> paramMap) {
        return sysDictDataService.queryDictDataList(paramMap);
    }

    @PostMapping("/insertDictData")
    @ApiOperation(value = "添加字典", notes = "添加字典")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dictDesc",value = "字典描述",paramType = "query", required = true),
            @ApiImplicitParam(name = "dictSort",value = "排序序号",paramType = "query", required = true),
            @ApiImplicitParam(name = "dictName",value = "字典名称",paramType = "query", required = true),
            @ApiImplicitParam(name = "dictTypeId",value = "字典类型ID",paramType = "query"),
            @ApiImplicitParam(name = "dictValue",value = "字典值",paramType = "query"),
            @ApiImplicitParam(name = "state",value = "状态 1-启用，2--停用",paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate",value = "创建时间",paramType = "query", required = true),
            @ApiImplicitParam(name = "updateDate",value = "更新时间",paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> insertDictData(@RequestBody Map<String,String> paramMap) {
        return sysDictDataService.insertDictData(paramMap);
    }

    @PostMapping("/updateDictData")
    @ApiOperation(value = "修改字典", notes = "修改字典")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId",value = "自增Id",paramType = "query", required = true),
            @ApiImplicitParam(name = "dictDesc",value = "字典描述",paramType = "query", required = true),
            @ApiImplicitParam(name = "dictSort",value = "排序序号",paramType = "query", required = true),
            @ApiImplicitParam(name = "dictName",value = "字典名称",paramType = "query", required = true),
            @ApiImplicitParam(name = "dictTypeId",value = "字典类型ID",paramType = "query"),
            @ApiImplicitParam(name = "dictValue",value = "字典值",paramType = "query"),
            @ApiImplicitParam(name = "state",value = "状态 1-启用，2--停用",paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate",value = "创建时间",paramType = "query", required = true),
            @ApiImplicitParam(name = "updateDate",value = "更新时间",paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> updateDictData(@RequestBody Map<String,String> paramMap) {
        return sysDictDataService.updateDictData(paramMap);
    }

    @PostMapping("/deleteDictData")
    @ApiOperation(value = "删除字典", notes = "删除字典")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds",value = "字符串拼接ID",paramType = "query", required = true),
    })
    public Map<String,Object> deleteDictData(@RequestBody Map<String,String> paramMap) {
        return sysDictDataService.deleteDictData(paramMap);
    }
}
