package com.whjz.manage.controller.system;

import com.whjz.manage.service.system.SysConfigService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：配置相关接口
 */

@Api(tags = {"配置管理"})
@RestController
@Slf4j
@RequestMapping("sysConfig")
public class SysConfigController extends AbstractControllerImpl {

    @Autowired
    private SysConfigService sysConfigService;

    @PostMapping("/queryConfigList")
    @ApiOperation(value = "查询配置列表", notes = "根据查询条件返回配置列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "configCode",value = "配置编号  筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "configName",value = "配置名称 筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "configValue",value = "配置值 筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "state",value = "状态 1-启用，2--停用 筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "pageNum",value = "当前页数",paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize",value = "查询记录数",paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage",value = "是否分页",paramType = "query", required = true),
    })
    public Map<String,Object> queryConfigList(@RequestBody Map<String,String> paramMap) {
        return sysConfigService.queryConfigList(paramMap);
    }

    @PostMapping("/insertConfig")
    @ApiOperation(value = "添加配置", notes = "添加配置")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "configCode",value = "配置编号",paramType = "query", required = true),
            @ApiImplicitParam(name = "configName",value = "配置名称",paramType = "query", required = true),
            @ApiImplicitParam(name = "configValue",value = "配置值",paramType = "query", required = true),
            @ApiImplicitParam(name = "remark",value = "备注",paramType = "query"),
            @ApiImplicitParam(name = "state",value = "状态 1-启用，2--停用",paramType = "query", required = true),
            @ApiImplicitParam(name = "delFlag",value = "删除标识",paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate",value = "创建时间",paramType = "query"),
            @ApiImplicitParam(name = "updateDate",value = "更新时间",paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> insertConfig(@RequestBody Map<String,String> paramMap) {
        return sysConfigService.insertConfig(paramMap);
    }

    @PostMapping("/updateConfig")
    @ApiOperation(value = "修改配置", notes = "修改配置")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId",value = "自增Id",paramType = "query", required = true),
            @ApiImplicitParam(name = "configCode",value = "配置编号",paramType = "query", required = true),
            @ApiImplicitParam(name = "configName",value = "配置名称",paramType = "query", required = true),
            @ApiImplicitParam(name = "configValue",value = "配置值",paramType = "query", required = true),
            @ApiImplicitParam(name = "remark",value = "备注",paramType = "query"),
            @ApiImplicitParam(name = "state",value = "状态 1-启用，2--停用",paramType = "query", required = true),
            @ApiImplicitParam(name = "delFlag",value = "删除标识",paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate",value = "创建时间",paramType = "query"),
            @ApiImplicitParam(name = "updateDate",value = "更新时间",paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> updateConfig(@RequestBody Map<String,String> paramMap) {
        return sysConfigService.updateConfig(paramMap);
    }

    @PostMapping("/deleteConfig")
    @ApiOperation(value = "删除配置", notes = "删除配置")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds",value = "字符串拼接ID",paramType = "query", required = true),
    })
    public Map<String,Object> deleteConfig(@RequestBody Map<String,String> paramMap) {
        return sysConfigService.deleteConfig(paramMap);
    }
}
