package com.whjz.manage.controller.system;

import com.whjz.manage.service.system.SysMenuService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-25
 * remarks：菜单相关接口
 */

@Api(tags = {"菜单管理"})
@RestController
@Slf4j
@RequestMapping("sysMenu")
public class SysMenuController extends AbstractControllerImpl {

    @Autowired
    private SysMenuService sysMenuService;

    @PostMapping("/queryMenuList")
    @ApiOperation(value = "查询菜单列表", notes = "根据查询条件返回菜单列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "menuCode",value = "菜单编码 筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "menuName",value = "菜单名称 筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "menuUrl",value = "菜单地址 筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "parentId",value = "父级菜单 筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "pageNum",value = "当前页数",paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize",value = "查询记录数",paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage",value = "是否分页",paramType = "query", required = true),
    })
    public Map<String,Object> queryMenuList(@RequestBody Map<String,String> paramMap) {
        return sysMenuService.queryMenuList(paramMap);
    }

    @PostMapping("/insertMenu")
    @ApiOperation(value = "添加菜单", notes = "添加菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "parentId",value = "父菜单Id(一级菜单此字段传-1)",paramType = "query", required = true),
            @ApiImplicitParam(name = "menuCode",value = "菜单编码",paramType = "query"),
            @ApiImplicitParam(name = "menuLogo",value = "菜单图标",paramType = "query"),
            @ApiImplicitParam(name = "menuName",value = "菜单名称",paramType = "query", required = true),
            @ApiImplicitParam(name = "menuSeq",value = "菜单序号",paramType = "query"),
            @ApiImplicitParam(name = "menuUrl",value = "菜单地址",paramType = "query", required = true),
            @ApiImplicitParam(name = "operator",value = "操作人",paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser",value = "创建人",paramType = "query", required = true),
            @ApiImplicitParam(name = "state",value = "菜单状态,1--启用，2--停用",paramType = "query"),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> insertMenu(@RequestBody Map<String,String> paramMap) {
        return sysMenuService.insertMenu(paramMap);
    }

    @PostMapping("/updateMenu")
    @ApiOperation(value = "修改菜单", notes = "修改菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId",value = "自增Id",paramType = "query", required = true),
            @ApiImplicitParam(name = "parentId",value = "父菜单Id(一级菜单此字段传-1)",paramType = "query", required = true),
            @ApiImplicitParam(name = "menuCode",value = "菜单编码",paramType = "query"),
            @ApiImplicitParam(name = "menuLogo",value = "菜单图标",paramType = "query"),
            @ApiImplicitParam(name = "menuName",value = "菜单名称",paramType = "query", required = true),
            @ApiImplicitParam(name = "menuSeq",value = "菜单序号",paramType = "query"),
            @ApiImplicitParam(name = "menuUrl",value = "菜单地址",paramType = "query", required = true),
            @ApiImplicitParam(name = "operator",value = "操作人",paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser",value = "创建人",paramType = "query", required = true),
            @ApiImplicitParam(name = "state",value = "菜单状态,1--启用，2--停用",paramType = "query"),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> updateMenu(@RequestBody Map<String,String> paramMap) {
        return sysMenuService.updateMenu(paramMap);
    }

    @PostMapping("/deleteMenu")
    @ApiOperation(value = "删除菜单", notes = "删除菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds",value = "字符串拼接ID",paramType = "query", required = true),
    })
    public Map<String,Object> deleteMenu(@RequestBody Map<String,String> paramMap) {
        return sysMenuService.deleteMenu(paramMap);
    }
}
