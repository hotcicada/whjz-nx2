package com.whjz.manage.controller.system;

import com.whjz.manage.service.system.SysRequestLogService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-24
 * remarks：日志相关接口
 */

@Api(tags = {"日志管理"})
@RestController
@Slf4j
@RequestMapping("sysRequestLog")
public class SysRequestLogController extends AbstractControllerImpl {

    @Autowired
    private SysRequestLogService sysRequestLogService;

    @PostMapping("/queryRequestLogList")
    @ApiOperation(value = "查询日志列表", notes = "根据查询条件返回日志列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "pageNum",value = "当前页数",paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize",value = "查询记录数",paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage",value = "是否分页",paramType = "query", required = true),
    })
    public Map<String,Object> queryRequestLogList(@RequestBody Map<String,String> paramMap) {
        return sysRequestLogService.queryRequestLogList(paramMap);
    }

    @PostMapping("/insertRequestLog")
    @ApiOperation(value = "添加日志", notes = "添加日志")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "channel",value = "渠道（01普通、02语音、03视频）",paramType = "query", required = true),
            @ApiImplicitParam(name = "consumerSeqNo",value = "前端流水",paramType = "query", required = true),
            @ApiImplicitParam(name = "backSeqNo",value = "后台流水号",paramType = "query", required = true),
            @ApiImplicitParam(name = "imageSeqNo",value = "影像流水",paramType = "query"),
            @ApiImplicitParam(name = "interfaceInfo",value = "接口信息",paramType = "query", required = true),
            @ApiImplicitParam(name = "operateUser",value = "操作用户",paramType = "query", required = true),
            @ApiImplicitParam(name = "requestParameters",value = "请求参数",paramType = "query"),
            @ApiImplicitParam(name = "requestSource",value = "请求来源",paramType = "query", required = true),
            @ApiImplicitParam(name = "requestUrl",value = "请求url",paramType = "query", required = true),
            @ApiImplicitParam(name = "responseResult",value = "返回结果",paramType = "query", required = true),
            @ApiImplicitParam(name = "responseState",value = "返回状态",paramType = "query"),
            @ApiImplicitParam(name = "time",value = "接口用时",paramType = "query"),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> insertRequestLog(@RequestBody Map<String,String> paramMap) {
        return sysRequestLogService.insertRequestLog(paramMap);
    }

    @PostMapping("/updateRequestLog")
    @ApiOperation(value = "修改日志", notes = "修改日志")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId",value = "自增Id",paramType = "query", required = true),
            @ApiImplicitParam(name = "channel",value = "渠道（01普通、02语音、03视频）",paramType = "query", required = true),
            @ApiImplicitParam(name = "consumerSeqNo",value = "前端流水",paramType = "query", required = true),
            @ApiImplicitParam(name = "backSeqNo",value = "后台流水号",paramType = "query", required = true),
            @ApiImplicitParam(name = "imageSeqNo",value = "影像流水",paramType = "query"),
            @ApiImplicitParam(name = "interfaceInfo",value = "接口信息",paramType = "query", required = true),
            @ApiImplicitParam(name = "operateUser",value = "操作用户",paramType = "query", required = true),
            @ApiImplicitParam(name = "requestParameters",value = "请求参数",paramType = "query"),
            @ApiImplicitParam(name = "requestSource",value = "请求来源",paramType = "query", required = true),
            @ApiImplicitParam(name = "requestUrl",value = "请求url",paramType = "query", required = true),
            @ApiImplicitParam(name = "responseResult",value = "返回结果",paramType = "query", required = true),
            @ApiImplicitParam(name = "responseState",value = "返回状态",paramType = "query"),
            @ApiImplicitParam(name = "time",value = "接口用时",paramType = "query"),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> updateRequestLog(@RequestBody Map<String,String> paramMap) {
        return sysRequestLogService.updateRequestLog(paramMap);
    }

    @PostMapping("/deleteRequestLog")
    @ApiOperation(value = "删除日志", notes = "删除日志")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds",value = "字符串拼接ID",paramType = "query", required = true),
    })
    public Map<String,Object> deleteRequestLog(@RequestBody Map<String,String> paramMap) {
        return sysRequestLogService.deleteRequestLog(paramMap);
    }
}
