package com.whjz.manage.controller.message;

import com.whjz.manage.service.message.ProductTypeService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-10-11
 * remarks：产品分类相关接口
 */
@Api(tags = {"产品分类管理"})
@RestController
@Slf4j
@RequestMapping("productType")
public class ProductTypeController extends AbstractControllerImpl {

    @Autowired
    private ProductTypeService productTypeService;

    @PostMapping("/queryProductTypeList")
    @ApiOperation(value = "查询产品分类列表", notes = "根据查询条件返回产品分类列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "产品分类编号 筛选条件", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "产品分类名称 分类名字不能超过100个 筛选条件", paramType = "query"),
            @ApiImplicitParam(name = "state", value = "状态 1-正常2-停用 筛选条件", paramType = "query"),
            @ApiImplicitParam(name = "pageNum", value = "当前页数", paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize", value = "查询记录数", paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage", value = "是否分页", paramType = "query", required = true),
    })
    public Map<String, Object> queryProductTypeList(@RequestBody Map<String, String> paramMap) {
        return productTypeService.queryProductTypeList(paramMap);
    }

    @PostMapping("/insertProductType")
    @ApiOperation(value = "添加产品分类", notes = "添加产品分类")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "产品分类编号", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "产品分类名称 分类名字不能超过100个", paramType = "query", required = true),
            @ApiImplicitParam(name = "parentId", value = "上级分类", paramType = "query"),
            @ApiImplicitParam(name = "productTypeDesc", value = "分类描述", paramType = "query"),
            @ApiImplicitParam(name = "state", value = "状态 1-正常2-停用", paramType = "query"),
            @ApiImplicitParam(name = "createUser", value = "录入人", paramType = "query"),
            @ApiImplicitParam(name = "createDate", value = "录入时间", paramType = "query"),
            @ApiImplicitParam(name = "updateUser", value = "修改人", paramType = "query", required = true),
            @ApiImplicitParam(name = "updateDate", value = "修改时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
    })
    public Map<String, Object> insertProductType(@RequestBody Map<String, String> paramMap) {
        return productTypeService.insertProductType(paramMap);
    }

    @PostMapping("/updateProductType")
    @ApiOperation(value = "修改产品分类", notes = "修改产品分类")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId", value = "自增ID", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "产品分类编号", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "产品分类名称 分类名字不能超过100个", paramType = "query", required = true),
            @ApiImplicitParam(name = "parentId", value = "上级分类", paramType = "query"),
            @ApiImplicitParam(name = "productTypeDesc", value = "分类描述", paramType = "query"),
            @ApiImplicitParam(name = "state", value = "状态 1-正常2-停用", paramType = "query"),
            @ApiImplicitParam(name = "createUser", value = "录入人", paramType = "query"),
            @ApiImplicitParam(name = "createDate", value = "录入时间", paramType = "query"),
            @ApiImplicitParam(name = "updateUser", value = "修改人", paramType = "query", required = true),
            @ApiImplicitParam(name = "updateDate", value = "修改时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
    })
    public Map<String, Object> updateProductType(@RequestBody Map<String, String> paramMap) {
        return productTypeService.updateProductType(paramMap);
    }

    @PostMapping("/deleteProductType")
    @ApiOperation(value = "删除产品分类", notes = "删除产品分类")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds", value = "字符串拼接ID", paramType = "query", required = true),
    })
    public Map<String, Object> deleteProductType(@RequestBody Map<String, String> paramMap) {
        return productTypeService.deleteProductType(paramMap);
    }
}
