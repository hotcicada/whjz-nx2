package com.whjz.manage.controller.message;

import com.whjz.manage.service.message.SelfRaisingPointService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-10-12
 * remarks：自提点相关接口
 */
@Api(tags = {"自提点管理"})
@RestController
@Slf4j
@RequestMapping("selfRaisingPoint")
public class SelfRaisingPointController extends AbstractControllerImpl {

    @Autowired
    private SelfRaisingPointService selfRaisingPointService;

    @PostMapping("/querySelfRaisingPointList")
    @ApiOperation(value = "查询自提点列表", notes = "根据查询条件返回自提点列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "自提点编号 筛选条件", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "自提点名称 筛选条件", paramType = "query"),
            @ApiImplicitParam(name = "address", value = "自提点地址 筛选条件", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "自提点电话 筛选条件", paramType = "query"),
            @ApiImplicitParam(name = "charge_person", value = "自提点负责人 筛选条件", paramType = "query"),
            @ApiImplicitParam(name = "chargePrsonPhone", value = "自提点负责人电话 筛选条件", paramType = "query"),
            @ApiImplicitParam(name = "pageNum", value = "当前页数", paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize", value = "查询记录数", paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage", value = "是否分页", paramType = "query", required = true),
    })
    public Map<String, Object> querySelfRaisingPointList(@RequestBody Map<String, String> paramMap) {
        return selfRaisingPointService.querySelfRaisingPointList(paramMap);
    }

    @PostMapping("/insertSelfRaisingPoint")
    @ApiOperation(value = "添加自提点", notes = "添加自提点")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "自提点编号", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "自提点名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "address", value = "自提点地址", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "自提点电话", paramType = "query"),
            @ApiImplicitParam(name = "chargePerson", value = "自提点负责人", paramType = "query"),
            @ApiImplicitParam(name = "chargePersonPhone", value = "自提点负责人电话", paramType = "query"),
            @ApiImplicitParam(name = "alternateContact", value = "自提点备用联系人", paramType = "query", required = true),
            @ApiImplicitParam(name = "alternateContactPhone", value = "自提点备用联系人电话", paramType = "query", required = true),
            @ApiImplicitParam(name = "businessLicense", value = "自提点营业执照信息", paramType = "query", required = true),
            @ApiImplicitParam(name = "legalPersonName", value = "自提点法人姓名", paramType = "query"),
            @ApiImplicitParam(name = "legalPersonPhone", value = "自提点法人电话", paramType = "query"),
            @ApiImplicitParam(name = "bankDeposit", value = "开户行", paramType = "query"),
            @ApiImplicitParam(name = "accountOpeningBranch", value = "开户网点", paramType = "query"),
            @ApiImplicitParam(name = "accountName", value = "户名", paramType = "query"),
            @ApiImplicitParam(name = "accountNumber", value = "账号", paramType = "query"),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query"),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query"),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext6", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext7", value = "扩展字段5", paramType = "query"),
    })
    public Map<String, Object> insertSelfRaisingPoint(@RequestBody Map<String, String> paramMap) {
        return selfRaisingPointService.insertSelfRaisingPoint(paramMap);
    }

    @PostMapping("/updateSelfRaisingPoint")
    @ApiOperation(value = "修改自提点", notes = "修改自提点")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId", value = "自增ID", paramType = "query"),
            @ApiImplicitParam(name = "code", value = "自提点编号", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "自提点名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "address", value = "自提点地址", paramType = "query"),
            @ApiImplicitParam(name = "phone", value = "自提点电话", paramType = "query"),
            @ApiImplicitParam(name = "chargePerson", value = "自提点负责人", paramType = "query"),
            @ApiImplicitParam(name = "chargePersonPhone", value = "自提点负责人电话", paramType = "query"),
            @ApiImplicitParam(name = "alternateContact", value = "自提点备用联系人", paramType = "query", required = true),
            @ApiImplicitParam(name = "alternateContactPhone", value = "自提点备用联系人电话", paramType = "query", required = true),
            @ApiImplicitParam(name = "businessLicense", value = "自提点营业执照信息", paramType = "query", required = true),
            @ApiImplicitParam(name = "legalPersonName", value = "自提点法人姓名", paramType = "query"),
            @ApiImplicitParam(name = "legalPersonPhone", value = "自提点法人电话", paramType = "query"),
            @ApiImplicitParam(name = "bankDeposit", value = "开户行", paramType = "query"),
            @ApiImplicitParam(name = "accountOpeningBranch", value = "开户网点", paramType = "query"),
            @ApiImplicitParam(name = "accountName", value = "户名", paramType = "query"),
            @ApiImplicitParam(name = "accountNumber", value = "账号", paramType = "query"),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query"),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query"),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext6", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext7", value = "扩展字段5", paramType = "query"),
    })
    public Map<String, Object> updateSelfRaisingPoint(@RequestBody Map<String, String> paramMap) {
        return selfRaisingPointService.updateSelfRaisingPoint(paramMap);
    }

    @PostMapping("/deleteSelfRaisingPoint")
    @ApiOperation(value = "删除自提点", notes = "删除自提点")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds", value = "字符串拼接ID", paramType = "query", required = true),
    })
    public Map<String, Object> deleteSelfRaisingPoint(@RequestBody Map<String, String> paramMap) {
        return selfRaisingPointService.deleteSelfRaisingPoint(paramMap);
    }
}
