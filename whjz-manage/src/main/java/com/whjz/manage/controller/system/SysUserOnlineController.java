package com.whjz.manage.controller.system;

import com.whjz.manage.service.system.SysUserOnlineService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：在线用户相关接口
 */

@Api(tags = {"在线用户管理"})
@RestController
@Slf4j
@RequestMapping("sysUserOnline")
public class SysUserOnlineController extends AbstractControllerImpl {

    @Autowired
    private SysUserOnlineService sysUserOnlineService;

    @PostMapping("/queryUserOnlineList")
    @ApiOperation(value = "查询在线用户列表", notes = "根据查询条件返回在线用户列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "pageNum", value = "当前页数", paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize", value = "查询记录数", paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage", value = "是否分页", paramType = "query", required = true),
    })
    public Map<String, Object> queryUserOnlineList(@RequestBody Map<String, String> paramMap) {
        return sysUserOnlineService.queryUserOnlineList(paramMap);
    }

    @PostMapping("/insertUserOnline")
    @ApiOperation(value = "添加在线用户", notes = "添加在线用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "loginName", value = "登录账号", paramType = "query", required = true),
            @ApiImplicitParam(name = "deptName", value = "部门名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "ipaddr", value = "登录IP地址", paramType = "query", required = true),
            @ApiImplicitParam(name = "loginLocation", value = "loginLocation", paramType = "query"),
            @ApiImplicitParam(name = "browser", value = "浏览器类型", paramType = "query", required = true),
            @ApiImplicitParam(name = "os", value = "操作系统", paramType = "query", required = true),
            @ApiImplicitParam(name = "states", value = "在线状态on_line在线off_line离线", paramType = "query"),
            @ApiImplicitParam(name = "startTimesTamp", value = "session创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "lastAccessTime", value = "session最后访问时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "expireTime", value = "超时时间，单位为分钟", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
    })
    public Map<String, Object> insertUserOnline(@RequestBody Map<String, String> paramMap) {
        return sysUserOnlineService.insertUserOnline(paramMap);
    }

    @PostMapping("/updateUserOnline")
    @ApiOperation(value = "修改在线用户", notes = "修改在线用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "sessionId", value = "用户会话id", paramType = "query", required = true),
            @ApiImplicitParam(name = "loginName", value = "登录账号", paramType = "query", required = true),
            @ApiImplicitParam(name = "deptName", value = "部门名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "ipaddr", value = "登录IP地址", paramType = "query", required = true),
            @ApiImplicitParam(name = "loginLocation", value = "loginLocation", paramType = "query"),
            @ApiImplicitParam(name = "browser", value = "浏览器类型", paramType = "query", required = true),
            @ApiImplicitParam(name = "os", value = "操作系统", paramType = "query", required = true),
            @ApiImplicitParam(name = "states", value = "在线状态on_line在线off_line离线", paramType = "query"),
            @ApiImplicitParam(name = "startTimesTamp", value = "session创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "lastAccessTime", value = "session最后访问时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "expireTime", value = "超时时间，单位为分钟", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
    })
    public Map<String, Object> updateUserOnline(@RequestBody Map<String, String> paramMap) {
        return sysUserOnlineService.updateUserOnline(paramMap);
    }

    @PostMapping("/deleteUserOnline")
    @ApiOperation(value = "删除在线用户", notes = "删除在线用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds", value = "字符串拼接ID", paramType = "query", required = true),
    })
    public Map<String, Object> deleteUserOnline(@RequestBody Map<String, String> paramMap) {
        return sysUserOnlineService.deleteUserOnline(paramMap);
    }
}
