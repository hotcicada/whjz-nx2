package com.whjz.manage.controller.system;

import com.whjz.manage.service.system.SysUserService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：用户相关接口
 */
@Api(tags = {"用户管理"})
@RestController
@Slf4j
@RequestMapping("sysUser")
public class SysUserController extends AbstractControllerImpl {

    @Autowired
    private SysUserService sysUserService;

    @PostMapping("/queryUserList")
    @ApiOperation(value = "查询用户列表", notes = "根据查询条件返回用户列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deptId",value = "部门 筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "userId",value = "用户账号 筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "userState",value = "用户状态 (1-正常 2-冻结 3-删除) 筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "phone",value = "用户手机号码 筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "startDate",value = "开始时间 筛选条件 (实际查询的是创建时间满足开始时间到结束时间的数据)",paramType = "query"),
            @ApiImplicitParam(name = "endDate",value = "结束时间 筛选条件 (实际查询的是创建时间满足开始时间到结束时间的数据)",paramType = "query"),
            @ApiImplicitParam(name = "pageNum",value = "当前页数",paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize",value = "查询记录数",paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage",value = "是否分页",paramType = "query", required = true),
    })
    public Map<String,Object> queryUserList(@RequestBody Map<String,String> paramMap) {
        return sysUserService.queryUserList(paramMap);
    }

    @PostMapping("/insertUser")
    @ApiOperation(value = "添加用户", notes = "添加用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deptId",value = "机构id",paramType = "query", required = true),
            @ApiImplicitParam(name = "userNetName",value = "用户名称",paramType = "query"),
            @ApiImplicitParam(name = "userName",value = "用户姓名",paramType = "query", required = true),
            @ApiImplicitParam(name = "userSex",value = "用户性别(1-男 2-女)",paramType = "query"),
            @ApiImplicitParam(name = "userState",value = "用户状态(1-正常 2-冻结 3-删除)",paramType = "query"),
            @ApiImplicitParam(name = "userIdNumber",value = "身份证号",paramType = "query"),
            @ApiImplicitParam(name = "phone",value = "用户手机号码",paramType = "query"),
            @ApiImplicitParam(name = "userPasswd",value = "用户密码",paramType = "query", required = true),
            @ApiImplicitParam(name = "operator",value = "最后操作人",paramType = "query", required = true),
            @ApiImplicitParam(name = "createrUser",value = "创建者",paramType = "query", required = true),
            @ApiImplicitParam(name = "currentAddress",value = "现居住地址",paramType = "query"),
            @ApiImplicitParam(name = "residenceAddress",value = "户籍地址",paramType = "query"),
            @ApiImplicitParam(name = "headTempUrl",value = "影像临时存储路径",paramType = "query"),
            @ApiImplicitParam(name = "ext1",value = "已使用（1-进行了密码重置  2-进行了密码修改） 扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
            @ApiImplicitParam(name = "ext6",value = "扩展字段5",paramType = "query"),
            @ApiImplicitParam(name = "ext7",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> insertUser(@RequestBody Map<String,String> paramMap) {
        return sysUserService.insertUser(paramMap);
    }

    @PostMapping("/updateUser")
    @ApiOperation(value = "修改用户", notes = "修改用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId",value = "自增ID",paramType = "query", required = true),
            @ApiImplicitParam(name = "deptId",value = "机构id",paramType = "query", required = true),
            @ApiImplicitParam(name = "userNetName",value = "用户名称",paramType = "query"),
            @ApiImplicitParam(name = "userName",value = "用户姓名",paramType = "query", required = true),
            @ApiImplicitParam(name = "userSex",value = "用户性别(1-男 2-女)",paramType = "query"),
            @ApiImplicitParam(name = "userState",value = "用户状态((1-正常 2-冻结 3-删除)",paramType = "query"),
            @ApiImplicitParam(name = "userIdNumber",value = "身份证号",paramType = "query"),
            @ApiImplicitParam(name = "phone",value = "用户手机号码",paramType = "query"),
            @ApiImplicitParam(name = "userPasswd",value = "用户密码",paramType = "query", required = true),
            @ApiImplicitParam(name = "operator",value = "最后操作人",paramType = "query", required = true),
            @ApiImplicitParam(name = "createrUser",value = "创建者",paramType = "query", required = true),
            @ApiImplicitParam(name = "currentAddress",value = "现居住地址",paramType = "query"),
            @ApiImplicitParam(name = "residenceAddress",value = "户籍地址",paramType = "query"),
            @ApiImplicitParam(name = "headTempUrl",value = "影像临时存储路径",paramType = "query"),
            @ApiImplicitParam(name = "ext1",value = "已使用（1-进行了密码重置  2-进行了密码修改） 扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
            @ApiImplicitParam(name = "ext6",value = "扩展字段5",paramType = "query"),
            @ApiImplicitParam(name = "ext7",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> updateUser(@RequestBody Map<String,String> paramMap) {
        return sysUserService.updateUser(paramMap);
    }

    @PostMapping("/deleteUser")
    @ApiOperation(value = "删除用户", notes = "删除用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds",value = "字符串拼接ID",paramType = "query", required = true),
    })
    public Map<String,Object> deleteUser(@RequestBody Map<String,String> paramMap) {
        return sysUserService.deleteUser(paramMap);
    }

    @PostMapping("/frozenUser")
    @ApiOperation(value = "冻结用户", notes = "冻结用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId",value = "自增ID",paramType = "query", required = true),
    })
    public Map<String,Object> frozenUser(@RequestBody Map<String,String> paramMap) {
        return sysUserService.frozenUser(paramMap);
    }

    @PostMapping("/resetUserPwd")
    @ApiOperation(value = "重置用户密码", notes = "重置用户密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId",value = "自增ID",paramType = "query", required = true),
    })
    public Map<String,Object> resetUserPwd(@RequestBody Map<String,String> paramMap) {
        return sysUserService.resetUserPwd(paramMap);
    }
}
