package com.whjz.manage.dao.system;

import com.whjz.entity.system.SysMenu;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-22
 * remarks：菜单相关接口
 */

public interface SysMenuMapper {
    //查询字典类别
    List<SysMenu> queryMenuList(Map<String, String> paramMap);

    //添加字典类别
    boolean insertMenu(Map<String, String> paramMap);

    //修改字典类别
    boolean modifyMenu(Map<String, String> paramMap);

    //删除字典类别
    boolean deleteMenu(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryMenuCount(Map<String, String> paramMap);

    //根据ID查询字典类别
    SysMenu queryMenuById(@Param("autoId") String autoId);
}
