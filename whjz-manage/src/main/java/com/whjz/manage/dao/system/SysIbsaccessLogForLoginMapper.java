package com.whjz.manage.dao.system;

import com.whjz.entity.system.SysIbsaccessLogForLogin;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-25
 * remarks：登录日志相关接口
 */

public interface SysIbsaccessLogForLoginMapper {
    //查询字典类别
    List<SysIbsaccessLogForLogin> queryIbsaccessLogForLoginList(Map<String, String> paramMap);

    //添加字典类别
    boolean insertIbsaccessLogForLogin(Map<String, String> paramMap);

    //修改字典类别
    boolean modifyIbsaccessLogForLogin(Map<String, String> paramMap);

    //删除字典类别
    boolean deleteIbsaccessLogForLogin(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryIbsaccessLogForLoginCount(Map<String, String> paramMap);

    //根据ID查询字典类别
    SysIbsaccessLogForLogin queryIbsaccessLogForLoginById(@Param("autoId") String autoId);
}
