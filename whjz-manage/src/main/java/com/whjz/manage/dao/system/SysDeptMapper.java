package com.whjz.manage.dao.system;

import com.whjz.entity.system.SysDept;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-22
 * remarks：机构相关接口
 */

public interface SysDeptMapper {
    //查询字典类别
    List<SysDept> queryDeptList(Map<String, String> paramMap);

    //添加字典类别
    boolean insertDept(Map<String, String> paramMap);

    //修改字典类别
    boolean modifyDept(Map<String, String> paramMap);

    //删除字典类别
    boolean deleteDept(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryDeptCount(Map<String, String> paramMap);

    //根据ID查询字典类别
    SysDept queryDeptById(@Param("autoId") String autoId);
}
