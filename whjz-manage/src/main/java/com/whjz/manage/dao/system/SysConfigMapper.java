package com.whjz.manage.dao.system;

import com.whjz.entity.system.SysConfig;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-22
 * remarks：配置相关接口
 */

public interface SysConfigMapper {
    //查询字典类别
    List<SysConfig> queryConfigList(Map<String, String> paramMap);

    //添加字典类别
    boolean insertConfig(Map<String, String> paramMap);

    //修改字典类别
    boolean modifyConfig(Map<String, String> paramMap);

    //删除字典类别
    boolean deleteConfig(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryConfigCount(Map<String, String> paramMap);

    //根据ID查询字典类别
    SysConfig queryConfigById(@Param("autoId") String autoId);
}
