package com.whjz.manage.dao.message;

import com.whjz.entity.message.BondStandard;
import feign.Param;

import java.util.List;
import java.util.Map;
/**
 *  author: hj
 *  date: 2020-10-10
 *  remarks：供应商保证金标准接口
 */
public interface BondStandardMapper {

    //查询供应商保证金标准
    List<BondStandard> queryBondStandardList(Map<String, String> paramMap);
    //增加供应商保证金标准
    boolean insertBondStandard(Map<String,String >paraMap);
    //修改供应商保证金标准
    boolean updateBondStandard(Map<String, String> parMap);
    //删除供应商保证金标准
    boolean deleteBondStandard(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryBondStandardCount(Map<String,String> paramMap);


}
