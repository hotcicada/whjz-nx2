package com.whjz.manage.dao.message;

import com.whjz.entity.message.Bond;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 *  author: hj
 *  date: 2020-10-9
 *  remarks：供应商保证金类相关接口
 */
public interface BondMapper{

    //查询供应商保证金
    List<Bond> queryBondList(Map<String, String> paramMap);
    //增加供应商保证金
    boolean insertBond(Map<String,String >paraMap);
    //修改供应商保证金
    boolean updateBond(Map<String, String> parMap);
    //删除供应商保证金
    boolean deleteBond(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryBondCount(Map<String,String> paramMap);


}
