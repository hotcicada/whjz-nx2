package com.whjz.manage.dao.system;

import com.whjz.entity.system.SysUserRole;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-23
 * remarks：机构相关接口
 */

public interface SysUserRoleMapper {
    //查询字典类别
    List<SysUserRole> queryUserRoleList(Map<String, String> paramMap);

    //添加字典类别
    boolean insertUserRole(Map<String, String> paramMap);

    //修改字典类别
    boolean modifyUserRole(Map<String, String> paramMap);

    //删除字典类别
    boolean deleteUserRole(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryUserRoleCount(Map<String, String> paramMap);

    //根据ID查询字典类别
    SysUserRole queryUserRoleById(@Param("autoId") String autoId);
}
