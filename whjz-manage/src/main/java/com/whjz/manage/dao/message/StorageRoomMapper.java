package com.whjz.manage.dao.message;

import com.whjz.entity.message.StorageRoom;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author:hj
 * date:2020-10-13
 * remarks:储货间接口
 */
public interface StorageRoomMapper {
    //查询储货间
    List<StorageRoom> queryStorageRoomList(Map<String, String> paramMap);
    //增加储货间
    boolean insertStorageRoom(Map<String,String >paraMap);
    //修改储货间
    boolean updateStorageRoom(Map<String, String> parMap);
    //删除储货间
    boolean deleteStorageRoom(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryStorageRoomCount(Map<String,String> paramMap);
}
