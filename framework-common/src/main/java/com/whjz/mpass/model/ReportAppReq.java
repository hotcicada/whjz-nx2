package com.whjz.mpass.model;

import lombok.Data;

/**
 * 设备上报请求类
 * @author youkun
 * @data 2018/7/6
 */
@Data
public class ReportAppReq {

    /**
     * 手机版本
     */
    private String appVersion;

    /**
     * 渠道
     */
    private String channel;

    /**
     * 手机网络类型 wifi
     */
    private String connectType;

    /**
     * 目标设备token
     */
    private String deliveryToken;

    /**
     * 手机的imei
     */
    private String imei;

    /**
     * 手机的imsi
     */
    private String imsi;

    /**
     *
     */
    private String model;

    /**
     * 设备类型 android类型不需要上报
     * 1为android
     * 2为ios
     *
     */
    private String osType;
}
