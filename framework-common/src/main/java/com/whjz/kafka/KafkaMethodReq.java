package com.whjz.kafka;

import lombok.Data;

import java.util.List;

/**
 * kafka调用方法请求参数
 * @author youkun
 * @data 2018/7/11
 */
@Data
public class KafkaMethodReq {

    /**
     * 方法名
     */
    private String method;

    /**
     * 类名
     */
    private Class className;

    /**
     * 参数
     */
    private List<KafkaParamsReq> paramsReqList;
}
