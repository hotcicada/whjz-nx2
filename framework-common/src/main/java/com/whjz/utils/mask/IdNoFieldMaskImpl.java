package com.whjz.utils.mask;

import java.util.Arrays;

public class IdNoFieldMaskImpl implements IFieldMaskable
{
    // true: 掩码；  false : 不掩码；
    private final boolean PHONE_NUM_MASK_FLAG = true;

    // 被淹码字段长度, 手机号， 固定电话（加区号）是 11 位长
    private final int PHONE_NUM_LEN = 18;



    // 被淹码字段第一位是 1，
    // eg.  1234567890ABCDEFGHI
    //      ID_NO_MASK_BEG_INDEX = 2
    //      ID_NO_MASK_END_INDEX = 18
    //      表示 从第二位 至 第 18 位掩码
    //      掩码结果为： 1******************I
    private final int PHONE_NUM_MASK_BEG_INDEX = 7;
    private final int PHONE_NUM_MASK_END_INDEX = 14;

    // 报文中所有表示手机号，电话号的字段名，按照逗号分隔， 全部添加在这个数组里
    private final String[] PHONE_NUM_FIELD = {"userIdNumber", "IdentNo","identNo", "IdentCrdNo", "SoclScrNo" };




    /**********  begin 单例 模式 返回对象 ****************************/
    private static IdNoFieldMaskImpl instance = new IdNoFieldMaskImpl();
    private IdNoFieldMaskImpl(){};
    public static IdNoFieldMaskImpl getInstance()
    {
        return instance;
    }
    /**********  end 单例 模式 返回对象 ****************************/

    @Override
    public boolean getMaskFlag()
    {
        return PHONE_NUM_MASK_FLAG;
    }

    @Override
    public int getFieldLen()
    {
        return PHONE_NUM_LEN;
    }

    @Override
    public int getMaskBeginIndex()
    {
        return PHONE_NUM_MASK_BEG_INDEX;
    }

    @Override
    public int getMaskEndIndex()
    {
        return PHONE_NUM_MASK_END_INDEX;
    }

    @Override
    public String[] getMaskFields()
    {
        return PHONE_NUM_FIELD;
    }

    @Override
    public String toString()
    {
        return "\nID_NO_MASK_FLAG=["+ PHONE_NUM_MASK_FLAG + "],\n"
                + "ID_NO_LEN=[" + PHONE_NUM_LEN + "],\n"
                + "ID_NO_MASK_BEG_INDEX=[" + PHONE_NUM_MASK_BEG_INDEX + "],\n"
                + "ID_NO_MASK_END_INDEX=[" + PHONE_NUM_MASK_END_INDEX + "],\n"
                + "ID_NO_FIELD=[" + Arrays.asList(PHONE_NUM_FIELD).toString() + "]\n";
    }
}
