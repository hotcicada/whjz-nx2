package com.whjz.utils.mask;

import java.util.Arrays;

public class PhoneNumFieldMaskImpl  implements IFieldMaskable
{
    // true: 掩码；  false : 不掩码；
    private final boolean ID_NO_MASK_FLAG = true;

    // 被淹码字段长度, 电话号、手机号是 11 位长
    private final int ID_NO_LEN = 11;



    // 被淹码字段第一位是 1，
    // eg.  1234567890ABCDEFGHI
    //      ID_NO_MASK_BEG_INDEX = 2
    //      ID_NO_MASK_END_INDEX = 18
    //      表示 从第二位 至 第 18 位掩码
    //      掩码结果为： 1******************I
    private final int ID_NO_MASK_BEG_INDEX = 4;
    private final int ID_NO_MASK_END_INDEX = 7;

    // 报文中所有表示身份证的字段名，按照逗号分隔， 全部添加在这个数组里
    private final String[] ID_NO_FIELD = {"phone", "telNo"};




    /**********  begin 单例 模式 返回对象 ****************************/
    private static PhoneNumFieldMaskImpl instance = new PhoneNumFieldMaskImpl();
    private PhoneNumFieldMaskImpl(){};
    public static PhoneNumFieldMaskImpl getInstance()
    {
        return instance;
    }
    /**********  end 单例 模式 返回对象 ****************************/

    @Override
    public boolean getMaskFlag()
    {
        return ID_NO_MASK_FLAG;
    }

    @Override
    public int getFieldLen()
    {
        return ID_NO_LEN;
    }

    @Override
    public int getMaskBeginIndex()
    {
        return ID_NO_MASK_BEG_INDEX;
    }

    @Override
    public int getMaskEndIndex()
    {
        return  ID_NO_MASK_END_INDEX;
    }

    @Override
    public String[] getMaskFields()
    {
        return ID_NO_FIELD;
    }

    @Override
    public String toString()
    {
        return "\nID_NO_MASK_FLAG=["+ID_NO_MASK_FLAG + "],\n"
                + "ID_NO_LEN=[" + ID_NO_LEN  + "],\n"
                + "ID_NO_MASK_BEG_INDEX=[" + ID_NO_MASK_BEG_INDEX  + "],\n"
                + "ID_NO_MASK_END_INDEX=[" +  ID_NO_MASK_END_INDEX + "],\n"
                + "ID_NO_FIELD=[" + Arrays.asList(ID_NO_FIELD).toString() + "]\n";
    }
}
