package com.whjz.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * Bean转map
 */
public class ObjectToMapUtil {

    private static final Logger log = LoggerFactory.getLogger(ObjectToMapUtil.class);

    public static Map<String, Object> objectToMap(Object object) {
        if (object == null) {
            return null;
        }
        Map<String, Object> map = new HashMap<String, Object>();
        Field[] fields = object.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                map.put(field.getName(), field.get(object));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                log.error("objectToMap error : " + e.getMessage());
            }
        }
        return map;
    }
}
