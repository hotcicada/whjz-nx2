package com.whjz.utils;

public final class StringConstantUtil {
	//系统层的公共常量
	/** add by czl redis集群后台流水号**/
	public static final String BACK_SEQ = "BACK_SEQ_";
	/** add by czl redis集群前端流水号**/
	public static final String FRONT_SEQ = "FRONT_SEQ_";
	/** add by czl redis请求路径**/
	public static final String WORKSTATION_RUL = "WORKSTATION_RUL_";
	/** add by czl redis集群后台流水号开关**/
	public static final String SWITCH_BACK_SEQ= "SWITCH_BACK_SEQ_";
	/** add by czl redis集群 用户密码错误次数**/
	public static final String USER_PASSWD_ERROR_COUNT="USER_PASSWD_ERROR_COUNT";
	/** add by czl redis集群 用户密码允许错误次数**/
	public static final int USER_PASSWD_ERROR_COUNT_EN=4;
	/** add by czl redis集群 用户密码最大不修改天数**/
	public static final String USER_PASSWD_MAX_UN="180";
	/** add by czl redis集群 用户密码需重置**/
	public static final String USER_PASSWD_UPDATETIME="99999999";
	/** add by czl 用户默认密码**/
	public static final String USER_PASSWD_DEFAULT="c2487458ddce071519b56fc322d568c7";
	/** add by czl 前端用户上送字段**/
	public static final String USER_DEFAULT="custNo";
	/** add by czl 前端用户上送字段**/
	public static final String USER_DEFAULT_UP="CustNo";
	/** add by czl 前端用户密码上送字段**/
	public static final String USER_PASSWD_FRONT_DEFAULT="userPasswd";
	/** add by czl 前端设备编号上送字段**/
	public static final String USER_DEVICEID_DEFAULT="deviceId";
	/** add by czl 前端渠道上送字段 2-工作站 3-后管**/
	public static final String USER_SOURCETYPE_DEFAULT="sourceType";
	/** add by fln 前端渠道上送字段 登录方式 1-密码 2-验证码  3-第三方**/
	public static final String USER_LOGIN_MODE="loginMode";
	/** add by czl 前端渠道上送字段 2-工作站 3-后管**/
	public static final String USER_SOURCETYPE_DEFAULT_UP="SourceType";
	/** add by czl 前端ip上送字段 **/
	public static final String USER_IP_DEFAULT="ip";
	/** add by czl 后台渠道类型 2-工作站 3-后管**/
	public static final String USER_SOURCETYPE_APP="2";
	/** add by czl 后台渠道类型 2-工作站 3-后管**/
	public static final String USER_SOURCETYPE_MANAGE="3";
	/** add by czl 短信模板业务短信01**/
	public static final String NOTE_01="01";
	/** add by czl 短信模板登录短信01**/
	public static final String NOTE_02="02";
	/** add by czl 远程授权 系统代号**/
	public static final String REMOTE_SYSTYPE="0006";
	/** add by czl 远程授权 图片类型 01-凭证 02-客户 03-柜员 04-身份证**/
	public static final String REMOTE_IMAGETYPE_01="01";
	/** add by czl 远程授权 图片类型 01-凭证 02-客户 03-柜员 04-身份证**/
	public static final String REMOTE_IMAGETYPE_02="02";
	/** add by czl 远程授权 图片类型 01-凭证 02-客户 03-柜员 04-身份证**/
	public static final String REMOTE_IMAGETYPE_03="03";
	/** add by czl 远程授权 图片类型 01-凭证 02-客户 03-柜员 04-身份证**/
	public static final String REMOTE_IMAGETYPE_04="04";
	/** add by czl 远程授权 图片类型 01-凭证 02-客户 03-柜员 04-身份证 05-截屏**/
	public static final String REMOTE_IMAGETYPE_05="05";
	/** add by czl 远程授权 影像流水号**/
	public static final String REMOTE_BACKNO="RemoteBackNo";
	/** add by czl 共享存储 图片类型 **/
	public static final String SAVE_IMAGETYPE=".png";
	/** add by czl 共享存储 图片类型 转txt **/
	public static final String SAVE_IMAGETYPE_TO_TXT=".txt";
	/** add by czl 业务流程表状态 1--提交,2--通过,3--拒绝,4--打回 5--远程授权提交成功 6--远程授权成功 **/
	public static final String BUSSINESS_STATUS_1="1";
	/** add by czl 业务流程表状态 1--提交,2--通过,3--拒绝,4--打回 5--远程授权提交成功 6--远程授权成功 **/
	public static final String BUSSINESS_STATUS_2="2";
	/** add by czl 业务流程表状态 1--提交,2--通过,3--拒绝,4--打回 5--远程授权提交成功 6--远程授权成功 **/
	public static final String BUSSINESS_STATUS_3="3";
	/** add by czl 业务流程表状态 1--提交,2--通过,3--拒绝,4--打回 5--远程授权提交成功 6--远程授权成功 **/
	public static final String BUSSINESS_STATUS_4="4";
	/** add by czl 业务流程表状态 1--提交,2--通过,3--拒绝,4--打回 5--远程授权提交成功 6--远程授权成功 **/
	public static final String BUSSINESS_STATUS_5="5";
	/** add by czl 业务流程表状态 1--提交,2--通过,3--拒绝,4--打回 5--远程授权提交成功 6--远程授权成功 **/
	public static final String BUSSINESS_STATUS_6="6";
	/** add by czl 硬加密-根秘钥 名称**/
	public static final String M4_BASE_KEY_NAME="YDYX.tmtran.zmk";
	/** add by czl 硬加密-根秘钥-秘钥值**/
//	public static final String M4_BASE_KEY_VALUE="3016745AB289EFCDBADCFE0325476981";//改为从config表中获取 硬加密-根秘钥-秘钥值
	/** add by czl 硬加密-转加密 核心秘钥名称**/
	public static final String M4_CHANGE_BASE_KEY="ZH.gmhost.zpk";
	/** add by czl 硬加密-根秘钥 公共秘钥名称**/
	public static final String M4_CHANGE_COMMON_KEY="ZH.gmpublic.zpk";
	/** add by czl 社保卡卡bin**/
	public static final String BIN_CARTE="62294788";
	/** add by czl 是否debug模式 0-否 1-录入数据 2-用录入的数据做挡板**/
	public static final String DEBUG_TYPE="0";



	public static final String USER = "USER_";
	public static final String  CUST = "CUST_ASSIST_";
	public static final String TOKEN = "TOKEN_";
	public static final String DEVICE = "DEVICE_";
	public static final String BOX = "BOX_";
	public static final String QRCODE = "QRCODE_";
	public static final String MSG = "MSG_";

	public static final String MDEVICE = "MDEVICE_";

	public static final String QUOTASINGLETRANSACTION="100000.00";//单笔交易限额
	public static final String QUOTAADDDAYTRANSACTION="100000.00";//日累计限额




	public static final String MANAGE_USER_ASSIST = "MANAGE_USER_ASSISTS_";
	public static final String MANAGE_TOKEN_ASSIST = "MANAGE_TOKEN_ASSIST_";
	public static final String MANAGE_DEVICE_ASSIST = "MANAGE_DEVICE_ASSIST_";
	/**   add by lcc  区分移动工作站和移动助理的key end*/

	/** add by gzx 共享存储 txt类型 **/
	public static final String SAVE_TXTTYPE=".txt";
	public static final String NOTE_TEMPLATE_TYPE="noteTemplateType"; //短信模板
	/** add by gzx 积分规则存放到Redis **/
	public static final String BUS_POINT_DESC_LIST = "BUS_POINT_DESC_LIST";

	/** add by gzx 机构权限 **/
	public static final String ORG_LIMIT = "orgLimit";
	public static final String ORG_CODE_LIST = "orgCodeList";//无权限查看
	public static final String ORG_LIMT_STATUS_YES = "1";//有权限查看
	public static final String ORG_LIMT_STATUS_NO = "2";//无权限查看

	/** add by gzx 机构权限 **/

	/** add by czl OKR 修改目标关键结果类型 1-个人 2-部门**/
	public static final String OKR_updateTargetDetail_type_1 ="1";
	/** add by czl OKR 修改目标关键结果类型 1-个人 2-部门**/
	public static final String OKR_updateTargetDetail_type_2 ="2";

	//登陆平台 1-APP  2-内管
	public static final String APPSOURCETYPE ="1";
	public static final String INNERTUBESOURCETYPE ="2";

	//1-进行了密码重置  2-进行了密码修改
	public static final String RESETPASSWORD = "1";
	public static final String CHANGEPASSWORD  = "2";

	//用户状态(1-正常 2-冻结 3-删除)
	public static final String USERSTATESNORMAL = "1";
	public static final String USERSTATESFROZEN = "2";
	public static final String USERSTATESDELETE = "3";

	//角色状态（1-启用,2-停用）
	public static final String ROLESTATESNORMAL = "1";
	public static final String ROLESTATESFSTOP = "2";

	//字典类别状态 (1--启用，2--停用)
	public static final String DICTTYPESTATESNORMAL = "1";
	public static final String DICTTYPESTATESSTOP = "2";

	//字典状态 （1--启用，2--停用）
	public static final String DICTDATASTATESNORMAL = "1";
	public static final String DICTDATASTATESSTOP = "2";

	//机构状态 (1-启用  2-停用)
	public static final String DEPTSTATESNORMAL = "1";
	public static final String DEPTSTATESSTOP = "2";

	//用户角色状态 (1-启用 2-停用)
	public static final String USERROLESTATESNORMAL = "1";
	public static final String USERROLESTATESSTOP = "2";

	//在线用户状态 (on_line-在线  off_line-离线)
	public static final String USERONLINESTATESONLINE = "on_line";
	public static final String USERONLINESTATESOFFLINE = "off_line";

	//角色菜单状态（1-启用  2-停用）
	public static final String ROLEMENUSTATESNORMAL = "1";
	public static final String ROLEMENUSTATESSTOP = "2";

	//菜单状态 (1--启用 2--停用)
	public static final String MENUSTATESNORMAL = "1";
	public static final String MENUSTATESSTOP = "2";

	//配置状态 （1--启用 2--停用）
	public static final String CONFIGSTATESNORMAL = "1";
	public static final String CONFIGSTATESSTOP = "2";

	//通知公告状态 （1--启用，2--停用）
	public static final String NOTICESTATESNORMAL = "1";
	public static final String NOTICESTATESSTOP = "2";
}
