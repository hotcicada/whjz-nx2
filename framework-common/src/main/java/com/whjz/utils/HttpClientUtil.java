package com.whjz.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
@Slf4j
public class HttpClientUtil {
	
	/**
	 * AI-语音	
	 * @return
	 */
	 public static String sendPost(String url,String xAppid,String curTime,String xParamBase64,String xCheckSum,byte[] fileData) {
		 
		 String result = "";
			BufferedReader in = null;
			OutputStream out = null;
			try {
				URL realUrl = new URL(url);
				HttpURLConnection connection = (HttpURLConnection)realUrl.openConnection();
				
				connection.setRequestProperty("X-Appid", xAppid);
				connection.setRequestProperty("X-CurTime", curTime);
				connection.setRequestProperty("X-Param", xParamBase64);
				connection.setRequestProperty("X-CheckSum", xCheckSum);
				
				connection.setDoOutput(true);
				connection.setDoInput(true);
				
				//connection.setConnectTimeout(20000);
				//connection.setReadTimeout(20000);
				try {
					out = connection.getOutputStream();
					out.write(fileData);
					out.flush();
				} catch (Exception e) {
					log.error("error :{}",e);
				}finally {
					if(out!=null){
						out.close();
					}
				}
				
				try {
					in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
					String line;
					while ((line = in.readLine()) != null) {
						result += line;
					}
				} catch (Exception e) {
					log.error("error :{}",e);
				}finally {
					if(in!=null){
						in.close();
					}
				}
			} catch (Exception e) {
				log.error("error :{}",e);
			}
			return result;
			}
		 

	

}
