package com.whjz.utils;

import java.util.Calendar;
import java.util.Map;
import java.util.Random;

/**
 * <p>Title: String2Utils</p>
 * <p>Description: 字符工具类</p>
 * @author xuzongtian
 * @date 2018年5月3日
 */
public class StringUtils {
	
    private static Random random;
    private static long seed;

    static {
        seed = System.currentTimeMillis();
        random = new Random(seed);
    }

    private static int uniform(int N) {
        return random.nextInt(N);
    }

    private static int uniform(int a, int b) {
        return a + uniform(b - a);
    }

	 /**
     * 生成20位token
     * 规则  获取当前时间戳（13位） + 随机数字、大小写字母（7位）
     * @return String
     */
    public static String getUUID() {
        int len = 7;
        char[] chArr = new char[len];
        chArr[0] = (char) ('0' + uniform(0, 10));
        chArr[1] = (char) ('A' + uniform(0, 26));
        chArr[2] = (char) ('a' + uniform(0, 26));

        char[] codes = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
                'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
                'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
                'y', 'z'};

        for (int i = 3; i < len; i++) {
            chArr[i] = codes[uniform(0, codes.length)];
        }

        for (int i = 0; i < len; i++) {
            int r = i + uniform(len - i);
            char temp = chArr[i];
            chArr[i] = chArr[r];
            chArr[r] = temp;
        }
        String s = new String(chArr);
        long timeInMillis = Calendar.getInstance().getTimeInMillis();
        s = timeInMillis + s;
        return s;
    }

    /**
     * 生成20位token
     * 规则  获取当前时间戳（13位） + 随机数字、大小写字母（len位）
     * @return String
     */
    public static String getUUID(int len) {
        char[] chArr = new char[len];
        chArr[0] = (char) ('0' + uniform(0, 10));
        chArr[1] = (char) ('A' + uniform(0, 26));
        chArr[2] = (char) ('a' + uniform(0, 26));

        char[] codes = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
                'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
                'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
                'y', 'z'};

        for (int i = 3; i < len; i++) {
            chArr[i] = codes[uniform(0, codes.length)];
        }

        for (int i = 0; i < len; i++) {
            int r = i + uniform(len - i);
            char temp = chArr[i];
            chArr[i] = chArr[r];
            chArr[r] = temp;
        }
        String s = new String(chArr);
        long timeInMillis = Calendar.getInstance().getTimeInMillis();
        s = timeInMillis + s;
        return s;
    }


    /**
     * 生成12+len 位token
     * 规则  获取当前时间yyyyMMddHHmmss + 随机数字、大小写字母（len位）
     * @return String
     */
    public static String getDateUUID(int len) {
        char[] chArr = new char[len];
        chArr[0] = (char) ('0' + uniform(0, 10));
        chArr[1] = (char) ('A' + uniform(0, 26));
        chArr[2] = (char) ('a' + uniform(0, 26));

        char[] codes = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
                'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
                'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
                'y', 'z'};

        for (int i = 3; i < len; i++) {
            chArr[i] = codes[uniform(0, codes.length)];
        }

        for (int i = 0; i < len; i++) {
            int r = i + uniform(len - i);
            char temp = chArr[i];
            chArr[i] = chArr[r];
            chArr[r] = temp;
        }
        String s = new String(chArr);
        String timeInMillis = DateUtils.getCurDate(DateUtils.yyyyMMddHHmmss_EN);
        s = timeInMillis + s;
        return s;
    }

    public static String getString(Object obj ){
        if(obj == null ){
            return null;
        }
        return obj.toString();
    }

    public static boolean isNotBlank(Map req, String key){
        boolean isnull = false;
        if(req.containsKey(key) && org.apache.commons.lang3.StringUtils.isNotBlank(getString(req.get(key)))){
            isnull = true;
        }
        return  isnull;
    }
    public static boolean isNotNullOrEmtory(String data){
        if (data!=null&&!"".equals(data)){
            return true;
        }
        return false;
    }
//    /**
//     * 根据key查询value
//     * 主要用于result返回的Map 取值
//     * @param map
//     * @return
//     */
//    public static String findValueByKey(Map map,Class className){
//        try {
//            Map result = (Map) map.get("data");
//
//        }catch (Exception e){
//            return  null;
//        }
//    }
 
}
