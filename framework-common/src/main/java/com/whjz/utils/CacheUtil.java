package com.whjz.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Title: CacheUtil</p>
 * <p>Description: 缓存</p>
 * @author xuzongtian
 * @date 2018年4月22日
 */
public class CacheUtil {

	private static CacheUtil cacheUtil;
	private static Map<String, Cache> map = new HashMap<String, Cache>();
	private static Map<String, String> map_config = new HashMap<String, String>();
	private static Map<String, Object> map_dict = new HashMap<String, Object>();
	private CacheUtil() {}
	
	/**
	 * <p>Title: getInstance</p>
	 * <p>Description: 单列</p>
	 * @return
	 */
	public static CacheUtil getInstance(){
		if(null == cacheUtil) {
			synchronized (CacheUtil.class) {
				if(null == cacheUtil)
					cacheUtil = new CacheUtil();
			}
		}
		return cacheUtil;
	}
	
	/**
	 * <p>Title: get</p>
	 * <p>Description: 获取缓存</p>
	 * @param key
	 * @return
	 */
	public Cache get(String key){
		return map.get(key);
	}
	
	/**
	 * <p>Title: add</p>
	 * <p>Description: 添加缓存</p>
	 * @param key
	 * @param cache
	 */
	public void add(String key, Cache cache) {
		map.put(key, cache);
	}
	
	/**
	 * <p>Title: remove</p>
	 * <p>Description: 删除缓存</p>
	 * @param key
	 */
	public void remove(String key) {
		map.remove(key);
	}

	public static Map<String, String> getMapConfig() {
		return map_config;
	}

	public static void setMapConfig(Map<String, String> map_config) {
		CacheUtil.map_config = map_config;
	}

	public static Map<String, Object> getMapDict() {
		return map_dict;
	}

	public static void setMapDict(Map<String, Object> map_dict) {
		CacheUtil.map_dict = map_dict;
	}
	
}
