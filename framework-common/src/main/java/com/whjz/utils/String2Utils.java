package com.whjz.utils;

import java.util.Calendar;
import java.util.Random;

/**
 * <p>Title: String2Utils</p>
 * <p>Description: 字符工具类</p>
 * @author xuzongtian
 * @date 2018年5月3日
 */
public class String2Utils {
	
    private static Random random;
    private static long seed;

    static {
        seed = System.currentTimeMillis();
        random = new Random(seed);
    }

    private static int uniform(int N) {
        return random.nextInt(N);
    }

    private static int uniform(int a, int b) {
        return a + uniform(b - a);
    }

	/**
	 * <p>Title: firstCharToLowerCase</p>
	 * <p>Description: 首字母变小写</p>
	 * @param str
	 * @return
	 */
	public static String firstCharToLowerCase(String str) {
        char firstChar = str.charAt(0);
        if (firstChar >= 'A' && firstChar <= 'Z') {
            char[] arr = str.toCharArray();
            arr[0] += ('a' - 'A');
            return new String(arr);
        }
        return str;
    }
	
	/**
	 * <p>Title: firstCharToUpperCase</p>
	 * <p>Description: 首字母变大写</p>
	 * @param str
	 * @return
	 */
	public static String firstCharToUpperCase(String str) {
        char firstChar = str.charAt(0);
        if (firstChar >= 'a' && firstChar <= 'z') {
            char[] arr = str.toCharArray();
            arr[0] -= ('a' - 'A');
            return new String(arr);
        }
        return str;
    }
	
	/**
	 * <p>Title: escapeXML</p>
	 * <p>Description: 消除转译字符</p>
	 * @param str
	 * @return
	 */
	public static String escapeXML(String str) {
        if (str == null)
            return "";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); ++i) {
            char c = str.charAt(i);
            switch (c) {
            case '\u00FF':
            case '\u0024':
                break;
            case '&':
                sb.append("&amp;");
                break;
            case '<':
                sb.append("&lt;");
                break;
            case '>':
                sb.append("&gt;");
                break;
            case '\"':
                sb.append("&quot;");
                break;
            case '\'':
                sb.append("&apos;");
                break;
            default:
                if (c >= '\u0000' && c <= '\u001F')
                    break;
                if (c >= '\uE000' && c <= '\uF8FF')
                    break;
                if (c >= '\uFFF0' && c <= '\uFFFF')
                    break;
                sb.append(c);
                break;
            }
        }
        return sb.toString();
    }
	
	
	
	 /**
     * 生成20位token
     * 规则  获取当前时间戳（13位） + 随机数字、大小写字母（7位）
     * @return String
     */
    public static String getGenerateUUID() {
        int len = 7;
        char[] chArr = new char[len];
        chArr[0] = (char) ('0' + uniform(0, 10));
        chArr[1] = (char) ('A' + uniform(0, 26));
        chArr[2] = (char) ('a' + uniform(0, 26));

        char[] codes = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
                'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
                'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd',
                'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
                'y', 'z'};

        for (int i = 3; i < len; i++) {
            chArr[i] = codes[uniform(0, codes.length)];
        }

        for (int i = 0; i < len; i++) {
            int r = i + uniform(len - i);
            char temp = chArr[i];
            chArr[i] = chArr[r];
            chArr[r] = temp;
        }
        String s = new String(chArr);
        long timeInMillis = Calendar.getInstance().getTimeInMillis();
        s = timeInMillis + s;
        return s;
    }
}
