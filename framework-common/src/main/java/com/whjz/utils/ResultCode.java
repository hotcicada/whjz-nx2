package com.whjz.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Title: ResultCode</p>
 * <p>Description: 业务码</p>
 * @author xuzongtian
 * @date 2018年4月25日
 */
public enum ResultCode {

	// 操作成功
	RESULT_CODE_SUCCESS("200", "交易成功"),//业务要求改成交易成功  交易失败
	// 操作失败
	RESUTL_CODE_FAILURE("500", "交易失败"),			//add by liweibin for (TASK)141 完整app请求流水日志  20190103
	// 系统异常
	RESUTL_CODE_EXCEPTION("-1", "系统异常"),			//add by liweibin for (TASK)141 完整app请求流水日志  20190103
	// 系统异常
	RESUTL_CODE_NOTUSE("52013", "背夹不能使用"),
	RESUTL_CODE_USE("52014", "背夹能使用"),

	RESUTL_SETTLE_ACCOUT("52015", "昨日未轧账，请先轧账!"),

	RESULT_CHECK_DATA("20001", "数据重复"),

	RESUTL_CODE_RElIEVE("52015", "未冻结"),

	//... 具体的业务返回码
	// 序列号
	RESULT_CODE_SEQUENCE_ERROR_DOES_NOT_EXIST("DoesNotExist", "序列号已使用完成");
	
	private String code;
	private String desc;
	
	private ResultCode(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}
	
	public String code() {
		return this.code;
	}
	
	public String desc() {
		return this.desc;
	}
	
	static Map<String, ResultCode> VALUES = new HashMap<String, ResultCode>();

	static {
		VALUES.put(RESULT_CODE_SUCCESS.code, RESULT_CODE_SUCCESS);
		VALUES.put(RESUTL_CODE_FAILURE.code, RESUTL_CODE_FAILURE);
	}
	
	public static ResultCode getResultCode(String key) {
		return VALUES.get(key);
	}
	
}
