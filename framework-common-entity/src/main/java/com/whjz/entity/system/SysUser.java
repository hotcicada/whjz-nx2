package com.whjz.entity.system;


import lombok.Data;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：用户实体类
 */

@Data
public class SysUser {
    private Integer autoId;  //自增Id
    private String userId; //用户编号id
    private String deptId; //用户机构org_id
    private String userNetName; //用户名称
    private String userName; //用户姓名
    private String userSex; //用户性别(1-男 2-女)
    private String userState; //用户状态((1-正常 2-冻结 3-删除)
    private String userIdKey; //用户唯一标识
    private String userIdNumber; //身份证号
    private String phone; //用户手机号码
    private String userPasswd; //用户密码
    private String userPasswdUpdate; //密码更新时间
    private String operator; //最后操作人
    private String createrUser; //创建者
    private String createDate; //创建时间
    private String updateDate; //修改时间
    private String currentAddress; //现居住地址
    private String residenceAddress; //户籍地址
    private String headTempUrl; //影像临时存储路径
    private String ext1; //已使用（1-进行了密码重置  2-进行了密码修改） 扩展字段1
    private String ext2; //扩展字段2
    private String ext3; //扩展字段3
    private String ext4; //扩展字段4
    private String ext5; //扩展字段5
    private String ext6;//扩展字段6
    private String ext7;//扩展字段7
}