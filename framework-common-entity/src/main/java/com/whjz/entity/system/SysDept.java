package com.whjz.entity.system;

import lombok.Data;

/**
 * author: fln
 * date: 2020-09-22
 * remarks：机构实体类
 */

@Data
public class SysDept {
    private Integer autoId;    // 自增Id
    private String deptCode;   //机构编号
    private String deptName;   //机构名称
    private Integer parentDeptId; //上级机构ID
    private String shortName;  //机构简称
    private String deptLevel;  //机构级别(0-总公司 1-分公司 3.部门)
    private String deptState;  //机构状态(1-启用  2-停用)
    private String deptType;   //机构类型
    private String contUser;   //负责人
    private String phonrNum;   //联系电话
    private String address; //地址
    private String postCode;   //邮编
    private String email;   //电子邮箱
    private String remark;  //备注
    private String createUser; //创建人
    private String updateUser; //修改人
    private String createDate; //创建时间
    private String updateDate; //更新时间
    private String ext1;    //扩展字段1
    private String ext2;    //扩展字段2
    private String ext3;    //扩展字段3
    private String ext4;    //扩展字段4
    private String ext5;    //扩展字段5
    private String ext6;    //扩展字段6
    private String ext7;    //扩展字段7
    private String ext8;    //扩展字段8
    private String ext9;    //扩展字段9
}
