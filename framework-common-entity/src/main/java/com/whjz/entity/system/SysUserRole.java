package com.whjz.entity.system;

import lombok.Data;

/**
 * author: fln
 * date: 2020-09-23
 * remarks：用户角色实体类
 */

@Data
public class SysUserRole {

    private Integer autoId;    //自增Id
    private String userId; //用户ID
    private String roleCode;   //角色ID
    private String operator;    //操作人
    private String createDate; //创建时间
    private String updateDate; //更新时间
    private String state;    //状态标识  1-启用  2-停用
    private String ext1;    //扩展字段1
    private String ext2;    //扩展字段2
    private String ext3;    //扩展字段3
    private String ext4;    //扩展字段4
    private String ext5;    //扩展字段5

}
