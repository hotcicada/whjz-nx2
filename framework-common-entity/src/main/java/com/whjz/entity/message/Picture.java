package com.whjz.entity.message;

import lombok.Data;

/**
 * author: fln
 * date: 2020-10-11
 * remarks：图片实体类
 */

@Data
public class Picture {
    private Integer autoId;     //自增ID
    private String channel;    //渠道
    private String frontSeqNo;    //前端流水号
    private String backSeqNo;    //后端流水号
    private String type;    //照片类型 1-产品标题图2-栏目图标3-banner图4-评价晒图5-身份证正面 6-身份证反面 7-合同文件 8-首页新用户送菜图 9-个人头像 10-人脸识别照片 12-人脸识别照片 13-营业执照 根据实际情况待添加
    private String fileType;    //文件类型 1-图片 2-pdf
    private String saveType;    //1-临时储存 2-永久储存
    private String address;    //储存地址
    private String keyword;    //储存关键字
    private String createUser;    //创建人
    private String createDeptId;    //创建机构id
    private String createDeptName;    //创建机构名称
    private String createDate;    //创建时间
    private String ext1;    //扩展字段1
    private String ext2;    //扩展字段2
    private String ext3;    //扩展字段3
    private String ext4;    //扩展字段4
    private String ext5;    //扩展字段5
}
