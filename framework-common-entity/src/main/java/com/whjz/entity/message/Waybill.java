package com.whjz.entity.message;

import lombok.Data;

/**
 * author:hj
 * date:2020-10-13
 * remarks:运单信息
 */
@Data
public class Waybill {
    /**
     * 序号
     */
    private int autoId;
    /**
     *运单编号
     */
    private String  waybillCode;
    /**
     *供应商编号
     */
    private String  supplierCode;
    /**
     *供应商名称
     */
    private String supplierName ;
    /**
     *分类名称
     */
    private String  waybillTypeName;
    /**
     *商品规格
     */
    private String  productSpecifications;
    /**
     *商品份数
     */
    private String copiesNumber ;
    /**
     *创建人
     */
    private String createUser;
    /**
     *创建时间
     */
    private String createDate;
    /**
     *扩展字段1
     */
    private String ext1;
    /**
     *扩展字段2
     */
    private String ext2;
    /**
     *扩展字段3
     */
    private String ext3;
    /**
     *扩展字段4
     */
    private String ext4;
    /**
     *扩展字段5
     */
    private  String ext5;
}
