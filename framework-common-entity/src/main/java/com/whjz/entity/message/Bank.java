package com.whjz.entity.message;

import lombok.Data;

/**
 * author:hj
 * date:2020-10-12
 * remarks:银行类
 */
@Data
public class Bank {

    /**
     *序号
     */
    private int  autoId;

    /**
     *银行编码
     */
    private  String  code;
    /**
     *银行名称
     */
    private  String name;
    /**
     *創建人
     */
    private  String createUser;
    /**
     *创建时间
     */
    private  String createDate ;
    /**
     *扩展字段1
     */
    private String ext1;
    /**
     *扩展字段2
     */
    private String ext2;
    /**
     *扩展字段3
     */
    private String ext3;
    /**
     *扩展字段4
     */
    private String ext4;
    /**
     *扩展字段5
     */
    private  String ext5;

}
