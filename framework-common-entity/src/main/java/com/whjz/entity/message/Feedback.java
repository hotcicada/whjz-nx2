package com.whjz.entity.message;

import lombok.Data;

/**
 * author:hj
 * date:2020-10-12
 * remarks:意见反馈类
 */
@Data
public class Feedback {
    /**
     * 序号
     */
    private  int autoId;
    /**
     *意见内容
     */
    private String content;
    /**
     *创建人
     */
    private String createUser;
    /**
     *创建时间
     */
    private String createDate;

    /**
     *状态
     */
    private String state;

    /**
     *扩展字段1
     */
    private String ext1;
    /**
     *扩展字段2
     */
    private String ext2;

    /**
     *扩展字段3
     */
    private String ext3;

    /**
     *扩展字段4
     */
    private String ext4;

    /**
     *扩展字段5
     */
    private String ext5;

}
