package com.whjz.entity.message;

import lombok.Data;

/**
 * author:hj
 * date:2020-10-11
 * remarks:常见问题类
 */
@Data
public class CommonProblem {
    /**
     *序号
     */
    private int autoId;
    /**
     *标题
     */
    private  String title;
    /**
     *内容
     */
    private  String content;
    /**
     *关键字
     */
    private  String keywords;
    /**
     *状态
     */
    private  String state;
    /**
     *创建人
     */
    private  String createUser;
    /**
     *创建时间
     */
    private  String createDate;
    /**
     *扩展字段1
     */
    private  String ext1;
    /**
     *扩展字段2
     */
    private  String ext2;
    /**
     *扩展字段3
     */
    private  String ext3;
    /**
     *扩展字段4
     */
    private  String ext4;
    /**
     *扩展字段5
     */
    private  String ext5;

}
