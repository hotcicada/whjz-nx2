package com.whjz.entity.message;

import lombok.Data;

/**
 * author: fln
 * date: 2020-10-10
 * remarks：评价实体类
 */

@Data
public class Evaluate {
    private Integer autoId;     //自增ID
    private String productCode;   //商品编号
    private String frontSeqNo;   //前端流水号
    private String backSeqNo;   //后端流水号
    private String content;    //评价内容
    private String evaluateDate;   //评价时间
    private String platformRatingStar;   //平台评价星级
    private String productRatingStar;    //产品评价星级
    private String comLeaderRatingStar;   //社区团长评价星级
    private String evaluateUser;  //评价人
    private String ext1;    //扩展字段1
    private String ext2;    //扩展字段2
    private String ext3;    //扩展字段3
    private String ext4;    //扩展字段4
    private String ext5;    //扩展字段5
}
