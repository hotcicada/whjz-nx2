package com.whjz.entity.message;

import lombok.Data;
/**
 *  author: hj
 *  date: 2020-10-10
 *  remarks：供应商保证金标准类
 */
@Data
public class BondStandard {
    /**
     * 序号
     */
    private int autoId;
    /**
     *保证金金额
     */
    private  String bondPrice;
    /**
     *供应商等级
     */
    private  String bondGroud;
    /**
     *备注
     */
    private  String remark;
    /**
     *创建人
     */
    private  String createUser;
    /**
     *创建时间
     */
    private  String createDate;

    /**
     *扩展字段1
     */
    private  String ext1;
    /**
     *扩展字段2
     */
    private  String ext2;
    /**
     *扩展字段3
     */
    private  String ext3;
    /**
     *扩展字段4
     */
    private  String ext4;
    /**
     *扩展字段5
     */
    private  String ext5;




}
