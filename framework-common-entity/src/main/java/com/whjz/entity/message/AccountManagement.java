package com.whjz.entity.message;

import lombok.Data;

/**
 * author: fln
 * date: 2020-10-12
 * remarks：账户实体类
 */

@Data
public class AccountManagement {
    private Integer autoId;    //自增id
    private Integer code;       //账户编号
    private Integer balance;    //余额
    private Integer createDate;    //账户创建时间
    private Integer validity;   //账户有效期
    private Integer state;  //状态 1-正常 2-冻结
    private Integer userCode;  //用户编号
    private Integer ext1;   //扩展字段1
    private Integer ext2;   //扩展字段2
    private Integer ext3;   //扩展字段3
    private Integer ext4;   //扩展字段4
    private Integer ext5;   //扩展字段5
}
