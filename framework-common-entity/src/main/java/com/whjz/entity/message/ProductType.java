package com.whjz.entity.message;

import lombok.Data;

/**
 * author: fln
 * date: 2020-10-11
 * remarks：产品分类实体类
 */

@Data
public class ProductType {
    private Integer autoId;     //自增ID
    private String code;   //产品分类编号
    private String name;   //产品分类名称 分类名字不能超过100个
    private String parentId;   //上级分类
    private String productTypeDesc;    //分类描述
    private String state;    //状态 1-正常2-停用
    private String createUser;    //录入人
    private String createDate;    //录入时间
    private String updateUser;    //修改人
    private String updateDate;    //修改时间
    private String ext1;    //扩展字段1
    private String ext2;    //扩展字段2
    private String ext3;    //扩展字段3
    private String ext4;    //扩展字段4
    private String ext5;    //扩展字段5
}
