package com.whjz.entity.message;

import lombok.Data;

/**
 * author:hj
 * date:2020-10-13
 * remarks:储货间
 */
@Data
public class StorageRoom {
    /**
     * 序号
     */
    private int autoId;
    /**
     *产品编号
     */
    private  String productCode;
    /**
     *产品名称
     */
    private  String productName;
    /**
     *产品分类
     */
    private  String productType;
    /**
     *商家ID
     */
    private  String supplierCode;
    /**
     *商家名称
     */
    private  String supplierName;
    /**
     *规格
     */
    private  String productSpecifications;
    /**
     *平台价格
     */
    private  String platformPrice;
    /**
     *超市价格
     */
    private  String supermarketPrice;
    /**
     *疯狂请购时间
     */
    private  String crazyRequisitionDate;
    /**
     *库存
     */
    private  String stock;
    /**
     *商品详情
     */
    private  String productDetails;
    /**
     *创建人
     */
    private  String createUser;

    /**
     *创建时间
     */
    private  String createDate;

    /**
     *扩展字段1
     */
    private String ext1;
    /**
     *扩展字段2
     */
    private String ext2;
    /**
     *扩展字段3
     */
    private String ext3;
    /**
     *扩展字段4
     */
    private String ext4;
    /**
     *扩展字段5
     */
    private  String ext5;



}