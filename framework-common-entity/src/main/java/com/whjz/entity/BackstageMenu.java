package com.whjz.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.hibernate.annotations.Table;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 后管菜单表
 * @author wangbin
 *
 */

@Data
@EqualsAndHashCode(callSuper = false)
@Entity(name = "mb_back_menu")
@Table(appliesTo = "mb_back_menu", comment = "后管菜单表")
public class BackstageMenu extends BaseModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6176053627919073960L;
	/**菜单名称**/

	private Integer autoId;

	/**菜单名称**/
	@ApiModelProperty(value = "菜单名称")
	@Column(name = "menu_name", columnDefinition = "varchar(50) DEFAULT NULL COMMENT '菜单名称' ")
	private String menuName;
	
	/**菜单地址**/
	@ApiModelProperty(value = "菜单地址")
	@Column(name = "menu_url", columnDefinition = "varchar(50) DEFAULT NULL COMMENT '菜单地址' ")
	private String menuUrl;
	
	/**菜单编码**/
	@ApiModelProperty(value = "菜单编码")
	@Column(name = "menu_code", columnDefinition = "varchar(50) DEFAULT NULL COMMENT '菜单编码' ")
	private String menuCode;
	
	/**菜单序号**/
	@ApiModelProperty(value = "菜单序号")
	@Column(name = "menu_seq", columnDefinition = "varchar(50) DEFAULT NULL COMMENT '菜单序号' ")
	private String menuSeq;
	
	/**父菜单Id**/
	@ApiModelProperty(value = "父菜单Id")
	@Column(name = "parent_id", columnDefinition = "varchar(50) DEFAULT NULL COMMENT '父菜单Id' ")
	private String parentId;
	
	/**菜单状态**/
	@ApiModelProperty(value = "菜单状态")
	@Column(name = "state", columnDefinition = "varchar(50) DEFAULT NULL COMMENT '菜单状态' ")
	private String state;
	
	/**操作人**/
	@ApiModelProperty(value = "操作人")
	@Column(name = "operator", columnDefinition = "varchar(50)  DEFAULT NULL COMMENT '操作人' ")
	private String operator;
	/**菜单图标 **/
	@ApiModelProperty(value="菜单图标")
	@Column(name="menu_logo",columnDefinition="varchar(50) DEFAULT NULL COMMENT '菜单图标'")
	private String menuLogo;
	
	//添加非数据库属性
	@Transient
	private List<BackstageMenu> children;


	@Transient
	private  Boolean isSelected;

	@Transient
	private String stateName;
}
