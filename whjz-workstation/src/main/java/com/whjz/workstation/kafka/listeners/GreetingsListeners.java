
package com.whjz.workstation.kafka.listeners;

import com.whjz.kafka.KafkaMethodReq;
import com.whjz.kafka.MethodAgain;
import com.whjz.workstation.kafka.interfaces.GreetingsChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;

@Slf4j
/*@Component

@EnableBinding({GreetingsChannel.class})*/
public class GreetingsListeners {
    @StreamListener(GreetingsChannel.INPUT)
    public void handleGreetings(KafkaMethodReq kafkaMethodReq){
    	log.info("队列消息接收");
        MethodAgain.KafkaMethodAgain(kafkaMethodReq);
    }
}

