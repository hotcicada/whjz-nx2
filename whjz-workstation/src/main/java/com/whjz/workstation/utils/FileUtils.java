package com.whjz.workstation.utils;

import com.whjz.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.UUID;

/**
 * @author lcc
 * @version 1.0
 * @description 文件上传工具
 * @date 2019/6/18 19:01
 */
@Slf4j
public class FileUtils {


    public static String uploadFile(MultipartFile file ,String videoPathUrl,String type){
        return uploadFile(file,videoPathUrl,type,false);
    }

    /**
     * @author ： lcc
     *  @DEsc :上传文件
     *  @date: 2019/5/25 12:37
     * @Param: null
     * @return
     */
    public static String uploadFile(MultipartFile file , String videoPathUrl, String type, boolean isVideo){
        String str = null;
        try {
            String fileName= URLDecoder.decode(file.getOriginalFilename(),"UTF-8");
            String prefix = fileName.substring(fileName.lastIndexOf("."));
            /**一下是将文件类型转变方便传输 开始*/

            String dateStr = DateUtils.getCurDate("yyyyMMdd");
            String dirPath = videoPathUrl;
           dirPath="D:/共享文件";
            if(StringUtils.isBlank(type)){
                type = prefix;
            }
            String filePath = dirPath + "/share/" + dateStr +"/"+ type;
            if("temp".equals(type)){
                filePath = dirPath +"/temp/" + dateStr +"/";
            }
            File dir = new File(filePath);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            log.info("文件所在目录:" + filePath);
            String uuid = UUID.randomUUID().toString();
            final File exceFile = File.createTempFile(uuid, prefix, dir);
            file.transferTo(exceFile);
            str = exceFile.getAbsolutePath();
            str = str.replace(videoPathUrl, "");
        } catch (IOException e) {
            log.error("上传文件异常",e);
        }
        return str ;
    }
}
