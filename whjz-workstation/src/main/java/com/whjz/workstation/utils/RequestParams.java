package com.whjz.workstation.utils;

/**
 * 请求变量(常量)
 */
public class RequestParams {
    public static final String PARAM_CUST_NO = "custNo";
    public static final String PARAM_DEVICE_ID = "deviceId";
    public static final String PARAM_TYPE = "type";
    public static final String PARAM_ITEM_TYPE = "itemType";// 大类
    public static final String PARAM_PROCESS_CODE = "processCode";// 流程编码
    public static final String PARAM_TASK_ID = "taskId";// 任务ID
    public static final String PARAM_APPROVE = "approve";// 同意
    public static final String PARAM_TASK_APPROVE = "taskApprove";// 审批处理结果
    public static final String PARAM_NEXT_NODE_ID = "nextNodeId";// 下个节点ID
    public static final String PARAM_NEXT_USER_ID = "nextUserId";// 下个用户ID
    public static final String PARAM_SOURCE = "source";// 来源,SourceTypeEnum
    public static final String PARAM_NODE_ID = "nodeId";// 节点ID
    public static final String PARAM_SUGGESTION = "suggestion";// 意见
    public static final String PARAM_BIG_CLASS_NO = "bigClassNo";// 业务类型
    public static final String PARAM_BUSS_TYPE = "bussType";// 业务类型
    public static final String PARAM_PK = "pk";// 数据pk
    public static final String PARAM_PRODUCT_CODE = "productCode";
    public static final String PARAM_NUMBER = "number";
    public static final String PARAM_SEQ_NO = "seqNo";
    public static final String PARAM_GUARANTEE_MODE = "guaranteeMode";
    public static final String PAGE_START = "pageStart";
    public static final String PAGE_END = "pageEnd";
    public static final String PAGE_SIZE = "pageSize";
    public static final String PAGE_NUM = "pageNum";
    public static final String PARAM_CLIENT_NO = "clientNo";
    public static final String PARAM_BRANCH_ID = "branchId";
//    public static final String PARAM_GLOBAL_TYPE = "globalType";
//    public static final String PARAM_GLOBAL_ID = "globalId";
    public static final String START_TIME = "startTime";
    public static final String FLAG = "flag";
    public static final String PARAM_CLIENT_TYPE = "clientType";
    public static final String PARAM_CUST_REQUEST_NO = "custRequestNo";
    public static final String PROCESS_ID = "processId";
    public static final String CARD_TYPE = "cardType";
    public static final String CARD_NO = "cardNo";
    public static final String FILE_NO = "fileNo";
    public static final String BUSI_FILE_TYPE = "busiFileType";
    public static final String USER_ID = "userId";
    public static final String FILE = "file";
    public static final String ORDER_NO = "orderNo";
    public static final String PARAM_FIRST_CATA = "firstCata";
    public static final String PARAM_SECOND_CATA = "secondCata";
    public static final String PARAM_LITTLE_CLASS_NO = "littleClassNo";
    public static final String PARAM_CONTRACT_NO_MAX = "contractNoMax";
    public static final String PARAM_LIWAI = "liwai";
    public static final String PARAM_NAMESPACE = "namespace";
}
