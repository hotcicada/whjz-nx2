package com.whjz.workstation.entity;

import com.whjz.entity.MbBankRole;
import lombok.Data;

import java.util.List;


@Data
public class MbBackUser {
    private Integer autoId;

    private String createDate;

    private String updateDate;

    private String userId;

    private String orgId;

    private String userName;

    private String userSex;

    private String userState;

    private String userIdKey;

    private String userIdNumber;

    private String phone;

    private String userPasswd;

    private String userPasswdUpdate;

    private String operator;

    private String creater;

    private String cUnitid;

    private String cJobid;

    private String currentAddress;

    private String residenceAddress;

    private String headImgId;

    private String headTempUrl;

    private String headTempDate;

    private String vuserId;

    private String vchrboxno;

    private String ext1;

    private String ext2;

    private String ext3;

    private String ext4;

    private String ext5;

    private String ext6;

    private String ext7;


    //新增的返回结果
    private String roleId;
    private String orgName;
    private String roleName;
    private String token;
    private String vuserOrgid;
    private String cUnitidName;
    private String userHeadUrl;
    private String ruleId;//考勤规则

    private String ruleIdBelongUser;//考勤规则

    public String getRuleIdBelongUser() {
        return ruleIdBelongUser;
    }

    public void setRuleIdBelongUser(String ruleIdBelongUser) {
        this.ruleIdBelongUser = ruleIdBelongUser;
    }

    public String getRuleId() {
        return ruleId;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    public String getUserHeadUrl() {
        return userHeadUrl;
    }

    public void setUserHeadUrl(String userHeadUrl) {
        this.userHeadUrl = userHeadUrl;
    }

    private List<MbBankRole> userRoleList;

    public List<MbBankRole> getUserRoleList() {
        return userRoleList;
    }

    public void setUserRoleList(List<MbBankRole> userRoleList) {
        this.userRoleList = userRoleList;
    }

    public String getcUnitidName() {
        return cUnitidName;
    }

    public void setcUnitidName(String cUnitidName) {
        this.cUnitidName = cUnitidName;
    }

    public String getVuserOrgid() {
        return vuserOrgid;
    }

    public void setVuserOrgid(String vuserOrgid) {
        this.vuserOrgid = vuserOrgid;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getOrgLevel() {
        return orgLevel;
    }

    public void setOrgLevel(String orgLevel) {
        this.orgLevel = orgLevel;
    }

    private String orgLevel;

    public Integer getAutoId() {
        return autoId;
    }

    public void setAutoId(Integer autoId) {
        this.autoId = autoId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate == null ? null : createDate.trim();
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate == null ? null : updateDate.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getUserSex() {
        return userSex;
    }

    public void setUserSex(String userSex) {
        this.userSex = userSex == null ? null : userSex.trim();
    }

    public String getUserState() {
        return userState;
    }

    public void setUserState(String userState) {
        this.userState = userState == null ? null : userState.trim();
    }

    public String getUserIdKey() {
        return userIdKey;
    }

    public void setUserIdKey(String userIdKey) {
        this.userIdKey = userIdKey == null ? null : userIdKey.trim();
    }

    public String getUserIdNumber() {
        return userIdNumber;
    }

    public void setUserIdNumber(String userIdNumber) {
        this.userIdNumber = userIdNumber == null ? null : userIdNumber.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getUserPasswd() {
        return userPasswd;
    }

    public void setUserPasswd(String userPasswd) {
        this.userPasswd = userPasswd == null ? null : userPasswd.trim();
    }

    public String getUserPasswdUpdate() {
        return userPasswdUpdate;
    }

    public void setUserPasswdUpdate(String userPasswdUpdate) {
        this.userPasswdUpdate = userPasswdUpdate == null ? null : userPasswdUpdate.trim();
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater == null ? null : creater.trim();
    }

    public String getcUnitid() {
        return cUnitid;
    }

    public void setcUnitid(String cUnitid) {
        this.cUnitid = cUnitid == null ? null : cUnitid.trim();
    }

    public String getcJobid() {
        return cJobid;
    }

    public void setcJobid(String cJobid) {
        this.cJobid = cJobid == null ? null : cJobid.trim();
    }

    public String getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress == null ? null : currentAddress.trim();
    }

    public String getResidenceAddress() {
        return residenceAddress;
    }

    public void setResidenceAddress(String residenceAddress) {
        this.residenceAddress = residenceAddress == null ? null : residenceAddress.trim();
    }

    public String getHeadImgId() {
        return headImgId;
    }

    public void setHeadImgId(String headImgId) {
        this.headImgId = headImgId == null ? null : headImgId.trim();
    }

    public String getHeadTempUrl() {
        return headTempUrl;
    }

    public void setHeadTempUrl(String headTempUrl) {
        this.headTempUrl = headTempUrl == null ? null : headTempUrl.trim();
    }

    public String getHeadTempDate() {
        return headTempDate;
    }

    public void setHeadTempDate(String headTempDate) {
        this.headTempDate = headTempDate == null ? null : headTempDate.trim();
    }

    public String getVuserId() {
        return vuserId;
    }

    public void setVuserId(String vuserId) {
        this.vuserId = vuserId == null ? null : vuserId.trim();
    }

    public String getVchrboxno() {
        return vchrboxno;
    }

    public void setVchrboxno(String vchrboxno) {
        this.vchrboxno = vchrboxno == null ? null : vchrboxno.trim();
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3 == null ? null : ext3.trim();
    }

    public String getExt4() {
        return ext4;
    }

    public void setExt4(String ext4) {
        this.ext4 = ext4 == null ? null : ext4.trim();
    }

    public String getExt5() {
        return ext5;
    }

    public void setExt5(String ext5) {
        this.ext5 = ext5 == null ? null : ext5.trim();
    }

    public String getExt6() {
        return ext6;
    }

    public void setExt6(String ext6) {
        this.ext6 = ext6 == null ? null : ext6.trim();
    }

    public String getExt7() {
        return ext7;
    }

    public void setExt7(String ext7) {
        this.ext7 = ext7 == null ? null : ext7.trim();
    }
}